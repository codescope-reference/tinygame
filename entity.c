EntityID NEXT_ID = 0;
EntityID get_next_id(void) {
    return NEXT_ID++;
}



Entity create_entity(Sprite *sprite, Vector2 position, int health, Rectangle collision_box) {
    Entity entity;
    entity.sprite = sprite;
    entity.position = position;
    entity.health = health;
    entity.is_attacking = false;
    entity.velocity = (Vector2){0, 0};
    entity.collision_box = collision_box;
    for (int i = 0; i < CO_COUNT; i++) {
        entity.co[i] = (coroutine_t){ 0 };
    }
    entity.is_dead = false;
    entity.is_invincible = false;
    entity.id = get_next_id();
    entity.attacks = createEntityIDAtkDict(256);
    // entity.registered_attacks = calloc(ATTACK_COUNT, sizeof(unsigned int));
    return entity;
}

void free_entity(Entity *entity) {
    // free(entity->registered_attacks);
    freeEntityIDAtkDict(entity->attacks);
}

Entity create_skeleton1(Vector2 position) {
    Sprite *sprite = load_sprite_char("skeleton1");
    Rectangle collision_box = (Rectangle){1.0 / 16.0, 1.0 / 16.0, 1.0 - 2.0 / 16.0, 1.0 - 2.0 / 16.0};
    Entity entity = create_entity(sprite, position, 10, collision_box);
    return entity;
}


void register_attack_on(Entity *target, Entity *attacker, int damage) {
    if (containsKeyEntityIDAtkDict(target->attacks, attacker->id)) {
        // Already has an attack registered
        // Attack will be deregistered after a certain delay
        return;
    } else {
        Attack atk = {attacker->id, damage, GetTime(), true};
        insertKVPEntityIDAtkDict(target->attacks, attacker->id, atk);
    }
}


Entity create_entity_from_id(int id, int x, int y) {
    // For each entity, load sprite, determine health, make collision box, and create entity
    int health = 10;
    Rectangle collision_box = (Rectangle){1.0 / 16.0, 1.0 / 16.0, 1.0 - 2.0 / 16.0, 1.0 - 2.0 / 16.0};
    switch (id) {
        case 4:
            {
            // Priest 1 v1
            char *names[] = {
                "assets/Character_animation/priests_idle/priest1/v1/priest1_v1_1.png",
                "assets/Character_animation/priests_idle/priest1/v1/priest1_v1_2.png",
                "assets/Character_animation/priests_idle/priest1/v1/priest1_v1_3.png",
                "assets/Character_animation/priests_idle/priest1/v1/priest1_v1_4.png",
            };
            Sprite *sprite = load_sprite((const char **)names, 4, 10);
            Entity entity = create_entity(sprite, (Vector2){x, y}, health, collision_box);
            return entity;
            }
        case 5:
            {
            // Priest 3 v1
            char *names[] = {
                "assets/Character_animation/priests_idle/priest3/v1/priest3_v1_1.png",
                "assets/Character_animation/priests_idle/priest3/v1/priest3_v1_2.png",
                "assets/Character_animation/priests_idle/priest3/v1/priest3_v1_3.png",
                "assets/Character_animation/priests_idle/priest3/v1/priest3_v1_4.png",
            };
            Sprite *sprite = load_sprite((const char **)names, 4, 10);
            Entity entity = create_entity(sprite, (Vector2){x, y}, health, collision_box);
            return entity;
            }
        case 6:
            {
            // Priest 2 v1
            char *names[] = {
                "assets/Character_animation/priests_idle/priest2/v1/priest2_v1_1.png",
                "assets/Character_animation/priests_idle/priest2/v1/priest2_v1_2.png",
                "assets/Character_animation/priests_idle/priest2/v1/priest2_v1_3.png",
                "assets/Character_animation/priests_idle/priest2/v1/priest2_v1_4.png",
            };
            Sprite *sprite = load_sprite((const char **)names, 4, 10);
            Entity entity = create_entity(sprite, (Vector2){x, y}, health, collision_box);
            return entity;
            }
        case 8:
            {
            // Skull v2
            char *names[] = {
                "assets/Character_animation/monsters_idle/skull/v2/skull_v2_1.png",
                "assets/Character_animation/monsters_idle/skull/v2/skull_v2_2.png",
                "assets/Character_animation/monsters_idle/skull/v2/skull_v2_3.png",
                "assets/Character_animation/monsters_idle/skull/v2/skull_v2_4.png",
            };
            Sprite *sprite = load_sprite((const char **)names, 4, 10);
            Entity entity = create_entity(sprite, (Vector2){x, y}, health, collision_box);
            return entity;
            }
        case 9:
            {
            // Vampire v2
            char *names[] = {
                "assets/Character_animation/monsters_idle/vampire/v2/vampire_v2_1.png",
                "assets/Character_animation/monsters_idle/vampire/v2/vampire_v2_2.png",
                "assets/Character_animation/monsters_idle/vampire/v2/vampire_v2_3.png",
                "assets/Character_animation/monsters_idle/vampire/v2/vampire_v2_4.png",
            };
            Sprite *sprite = load_sprite((const char **)names, 4, 10);
            Entity entity = create_entity(sprite, (Vector2){x, y}, health, collision_box);
            return entity;
            }
        case 11:
            {
            // Skeleton 2 v2
            char *names[] = {
                "assets/Character_animation/monsters_idle/skeleton2/v2/skeleton2_v2_1.png",
                "assets/Character_animation/monsters_idle/skeleton2/v2/skeleton2_v2_2.png",
                "assets/Character_animation/monsters_idle/skeleton2/v2/skeleton2_v2_3.png",
                "assets/Character_animation/monsters_idle/skeleton2/v2/skeleton2_v2_4.png",
            };
            Sprite *sprite = load_sprite((const char **)names, 4, 10);
            Entity entity = create_entity(sprite, (Vector2){x, y}, health, collision_box);
            return entity;
            }
        case 13:
            {
            // Skeleton 1 v2
            char *names[] = {
                "assets/Character_animation/monsters_idle/skeleton1/v2/skeleton1_v2_1.png",
                "assets/Character_animation/monsters_idle/skeleton1/v2/skeleton1_v2_2.png",
                "assets/Character_animation/monsters_idle/skeleton1/v2/skeleton1_v2_3.png",
                "assets/Character_animation/monsters_idle/skeleton1/v2/skeleton1_v2_4.png",
            };
            Sprite *sprite = load_sprite((const char **)names, 4, 10);
            Entity entity = create_entity(sprite, (Vector2){x, y}, health, collision_box);
            return entity;
            }
        default:
            {
            assert(0 && "Should never happen");
            break;
            }
    }
}
