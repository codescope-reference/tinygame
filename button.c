#include "raylib.h"
#include <stdbool.h>


// Return whether the button was clicked
bool button(Rectangle rect, const char *button_text, Color button_color, Color button_hover_color, Color text_color) {
    int margin_y = 0.1 * rect.height;
    int font_size = rect.height - 2 * margin_y;
    int margin_x = 0.1 * rect.width;
    int text_length;
    while (font_size > 0) {
        text_length = MeasureText(button_text, font_size);

        if (text_length < rect.width - 2 * margin_x) {
            break;
        }

        font_size -= 1;
    }


    Vector2 mouse_pos = GetMousePosition();

    bool hovered = CheckCollisionPointRec(mouse_pos, rect);
    Color active_color = hovered ? button_hover_color : button_color;
    DrawRectangleRec(rect, active_color);

    bool clicked = hovered && IsMouseButtonReleased(MOUSE_LEFT_BUTTON);




    int left_over_y = rect.height - font_size;
    margin_y = left_over_y / 2;
    int left_over_x = rect.width - text_length;
    margin_x = left_over_x / 2;
    DrawText(button_text, rect.x + margin_x, rect.y + margin_y, font_size, text_color);


    return clicked;
}


bool outline_button(Rectangle rect, Color outline_color, float outline_thickness, bool do_outline) {
    Vector2 mouse_pos = GetMousePosition();

    bool hovered = CheckCollisionPointRec(mouse_pos, rect);
    bool clicked = hovered && IsMouseButtonReleased(MOUSE_LEFT_BUTTON);

    if (do_outline) {
        DrawRectangleLinesEx(rect, outline_thickness, outline_color);
    }

    return clicked;
}
