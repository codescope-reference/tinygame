#include "editor.h"

#define OUTLINE_COLOR RED 
#define BUTTON_COLOR (Color){190, 190, 190, 255}
#define BUTTON_HOVER_COLOR WHITE
#define BUTTON_TEXT_COLOR BLACK

// Special editor state
typedef enum {
    EDITOR_NONE,
    EDITOR_LEFT_WALL_SELECTED,
    EDITOR_TOP_WALL_SELECTED,
    EDITOR_RIGHT_WALL_SELECTED,
    EDITOR_BOTTOM_WALL_SELECTED,
    EDITOR_BOTTOM_LEFT_CORNER_SELECTED,
    EDITOR_BOTTOM_RIGHT_CORNER_SELECTED,
    EDITOR_FLOOR_SELECTED,
    EDITOR_COLLISION_BOX_SELECTED,
    EDITOR_STARTING_POINT_SELECTED,
    EDITOR_CHARACTER_SELECTED,
    EDITOR_ITEM_SELECTED,
} EditorSelection; 

static EditorSelection editor_selection = EDITOR_NONE;
static int editor_character_selected_index = 0;
static int editor_item_selected_index = 0;

static int editor_x_offset = 0;
static int editor_y_offset = 0;


bool tile_is_selected(void) {
    return (editor_selection == EDITOR_LEFT_WALL_SELECTED 
            || editor_selection == EDITOR_TOP_WALL_SELECTED 
            || editor_selection == EDITOR_RIGHT_WALL_SELECTED
            || editor_selection == EDITOR_BOTTOM_WALL_SELECTED
            || editor_selection == EDITOR_BOTTOM_LEFT_CORNER_SELECTED
            || editor_selection == EDITOR_BOTTOM_RIGHT_CORNER_SELECTED
            || editor_selection == EDITOR_FLOOR_SELECTED
            );
}

int get_tile_index(void) {
    switch (editor_selection) {
        case EDITOR_LEFT_WALL_SELECTED:
        {
            int indices[] = {0, 10, 20, 30};
            int choice = GetRandomValue(0, 3);
            return indices[choice];
        }
        case EDITOR_TOP_WALL_SELECTED:
        {
            int indices[] = {1, 2, 3, 4};
            int choice = GetRandomValue(0, 3);
            return indices[choice];
        }
        case EDITOR_RIGHT_WALL_SELECTED:
        {
            int indices[] = {5, 15, 25, 35};
            int choice = GetRandomValue(0, 3);
            return indices[choice];
        }
        case EDITOR_BOTTOM_WALL_SELECTED:
        {
            int indices[] = {41, 42, 43, 44};
            int choice = GetRandomValue(0, 3);
            return indices[choice];
        }
        case EDITOR_BOTTOM_LEFT_CORNER_SELECTED: return 40;
        case EDITOR_BOTTOM_RIGHT_CORNER_SELECTED: return 45;
        case EDITOR_FLOOR_SELECTED:
        {
            int indices[] = {6  , 7  , 8  , 9  ,
                             16 , 17 , 18 , 19 ,
                             26 , 27 , 28 , 29 ,
                             // 36 , 37 , 38 , 39
            };
            int choice = GetRandomValue(0, 11);
            return indices[choice];
        }
        default:
            return -1;
    }
}

const char *save_folder = "levels";

static bool dir_already_read = false;
#define FILES_CAP 128
#define NAME_CAP 256 
static char files[FILES_CAP][NAME_CAP];
static int n_files = 0;

#define INPUT_CAP 256
static char input_field[INPUT_CAP] = {0};
static int input_ptr = 0;
static char temp_char[INPUT_CAP + 7] = {0};

static bool rectangle_drawing_active = false;
static Vector2 rectangle_start = {0};
static Vector2 rectangle_end = {0};

static bool editor_load_level_open = false;
static bool editor_save_level_open = false;


void editor_save(const char *filename, Editor * editor);
void editor_load(const char *filename, Editor *editor);


void reset_dir_listing(void) {
    dir_already_read = false;
    n_files = 0;
}

void populate_dir_listing(void) {
    DIR *d;
    struct dirent *dir;
    d = opendir(save_folder);
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (dir->d_type != DT_REG) { // Check it's a file. TODO: What does DT_REG mean?
                continue;
            }
            int char_ptr = 0;
            // Copy string
            char *ch = dir->d_name;
            while (*ch != '\0' && char_ptr < NAME_CAP - 1) {
                files[n_files][char_ptr] = *ch;

                ch++;
                char_ptr++;
            }
            files[n_files][char_ptr] = '\0';

            // Increment files counter
            n_files++;

            if (n_files >= FILES_CAP) {
                break;
            }
        }
    }
    closedir(d);
}


void place_wall(int x, int y, int tile_index, Editor *editor) {
    editor->tile_placements[editor->n_tiles][0] = tile_index;
    editor->tile_placements[editor->n_tiles][1] = x;
    editor->tile_placements[editor->n_tiles][2] = y;
    editor->n_tiles++;

    editor->collision_boxes[editor->n_collision_boxes].x = x;
    editor->collision_boxes[editor->n_collision_boxes].y = y;
    editor->n_collision_boxes++;
}


void split_rectangle_at(Rectangle src, Rectangle *panel1, Rectangle *panel2, float at, bool vertical_split) {
    if (vertical_split) {
        panel1->width = src.width * at;
        panel1->height = src.height;
    } else {
        panel1->width = src.width;
        panel1->height = src.height * at;
    }
    panel1->x = src.x;
    panel1->y = src.y;

    if (vertical_split) {
        panel2->width = src.width * (1 - at);
        panel2->height = src.height;
        panel2->x = src.x + src.width * at;
        panel2->y = src.y;
    } else {
        panel2->width = src.width;
        panel2->height = src.height * (1 - at);
        panel2->x = src.x;
        panel2->y = src.y + src.height * at;
    }
}


void draw_collision_box_selector(Rectangle rect, Editor *editor) {

    // Draw a rectangle with a red X in it
    DrawRectangleLinesEx(rect, 1.0f, RED);
    if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON) && CheckCollisionPointRec(GetMousePosition(), rect)) {
        if (editor_selection != EDITOR_COLLISION_BOX_SELECTED) {
            editor_selection = EDITOR_COLLISION_BOX_SELECTED;
        } else {
            editor_selection = EDITOR_NONE;
        }
    }
    if (editor_selection == EDITOR_COLLISION_BOX_SELECTED) {
        DrawLine(rect.x, rect.y, rect.x + rect.width, rect.y + rect.height, RED);
        DrawLine(rect.x + rect.width, rect.y, rect.x, rect.y + rect.height, RED);
    }
}


void draw_starting_point_selector(Rectangle rect, Editor *editor) {
    DrawRectangleLinesEx(rect, 1.0f, GREEN);
    if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON) && CheckCollisionPointRec(GetMousePosition(), rect)) {
        if (editor_selection != EDITOR_STARTING_POINT_SELECTED) {
            editor_selection = EDITOR_STARTING_POINT_SELECTED;
        } else {
            editor_selection = EDITOR_NONE;
        }
    }

    if (editor_selection == EDITOR_STARTING_POINT_SELECTED) {
        DrawCircle(rect.x + rect.width / 2.0f, rect.y + rect.height / 2.0f, rect.width / 2.0f, GREEN);
    }
}


int item_ids[] = {TILE_DOOR_1, TILE_DOOR_2, TILE_SIDE_DOOR_1};


bool item_ids_contains(int value) {
    for (size_t i = 0; i < sizeof(item_ids) / sizeof(int); i++) {
        if (item_ids[i] == value) {
            return true;
        }
    }
    return false;
}


void draw_item(Rectangle location, int item_index) {
    int item_x = item_index % 10;
    int item_y = item_index / 10;
    Rectangle src = {item_x * TILE_SIZE, item_y * TILE_SIZE, TILE_SIZE, TILE_SIZE};
    DrawTexturePro(graphics_data.tileset_texture, src, location, (Vector2){0, 0}, 0, WHITE);

    if (outline_button(location, OUTLINE_COLOR, 5.0, editor_selection == EDITOR_ITEM_SELECTED && editor_item_selected_index == item_index)) {
        editor_selection = EDITOR_ITEM_SELECTED;
        editor_item_selected_index = item_index;
    }
}


void draw_tile_selector(Rectangle panel, Editor *editor, GameState *state) {
    int n_cols = 5; // 10;
    float tile_size = panel.width / (float)n_cols;

    // Draw left vertical wall
    Rectangle tile = {panel.x, panel.y, tile_size, tile_size};
    Rectangle room_src = {0, 0, TILE_SIZE, TILE_SIZE};
    DrawTexturePro(graphics_data.tileset_texture, room_src, tile, (Vector2){0, 0}, 0, WHITE);
    if (outline_button(tile, OUTLINE_COLOR, 5.0, editor_selection == EDITOR_LEFT_WALL_SELECTED)) {
        editor_selection = EDITOR_LEFT_WALL_SELECTED;
    }

    // Draw top wall
    tile.x += tile_size;
    room_src.x = TILE_SIZE;
    DrawTexturePro(graphics_data.tileset_texture, room_src, tile, (Vector2){0, 0}, 0, WHITE);
    if (outline_button(tile, OUTLINE_COLOR, 5.0, editor_selection == EDITOR_TOP_WALL_SELECTED)) {
        editor_selection = EDITOR_TOP_WALL_SELECTED;
    }

    // Draw right vertical wall
    tile.x += tile_size;
    room_src.x += TILE_SIZE * 4;
    DrawTexturePro(graphics_data.tileset_texture, room_src, tile, (Vector2){0, 0}, 0, WHITE);
    if (outline_button(tile, OUTLINE_COLOR, 5.0, editor_selection == EDITOR_RIGHT_WALL_SELECTED)) {
        editor_selection = EDITOR_RIGHT_WALL_SELECTED;
    }

    // Draw bottom wall
    tile.x += tile_size;
    room_src.x = TILE_SIZE;
    room_src.y += TILE_SIZE * 4;
    DrawTexturePro(graphics_data.tileset_texture, room_src, tile, (Vector2){0, 0}, 0, WHITE);
    if (outline_button(tile, OUTLINE_COLOR, 5.0, editor_selection == EDITOR_BOTTOM_WALL_SELECTED)) {
        editor_selection = EDITOR_BOTTOM_WALL_SELECTED;
    }

    // Draw bottom left corner
    tile.x = 0;
    tile.y += tile_size;
    room_src.x = 0;
    room_src.y = TILE_SIZE * 4;
    DrawTexturePro(graphics_data.tileset_texture, room_src, tile, (Vector2){0, 0}, 0, WHITE);
    if (outline_button(tile, OUTLINE_COLOR, 5.0, editor_selection == EDITOR_BOTTOM_LEFT_CORNER_SELECTED)) {
        editor_selection = EDITOR_BOTTOM_LEFT_CORNER_SELECTED;
    }

    // Draw bottom right corner
    tile.x += tile_size;
    room_src.x += TILE_SIZE * 5;
    DrawTexturePro(graphics_data.tileset_texture, room_src, tile, (Vector2){0, 0}, 0, WHITE);
    if (outline_button(tile, OUTLINE_COLOR, 5.0, editor_selection == EDITOR_BOTTOM_RIGHT_CORNER_SELECTED)) {
        editor_selection = EDITOR_BOTTOM_RIGHT_CORNER_SELECTED;
    }


    // Draw floor tile
    tile.x += tile_size;
    room_src.x = TILE_SIZE * 6;
    room_src.y = 0;
    DrawTexturePro(graphics_data.tileset_texture, room_src, tile, (Vector2){0, 0}, 0, WHITE);
    if (outline_button(tile, OUTLINE_COLOR, 5.0, editor_selection == EDITOR_FLOOR_SELECTED)) {
        editor_selection = EDITOR_FLOOR_SELECTED;
    }

    tile.x = panel.x;
    tile.y += tile_size;
    draw_collision_box_selector(tile, editor);
    tile.x += tile_size;
    draw_starting_point_selector(tile, editor);

    
    // Draw character select
    int char_start_y = tile.y + tile_size;
    tile.x = panel.x;
    tile.y += tile_size;
    int orig_size = tile_size;
    tile_size /= 1.2;
    tile.width = tile_size;
    tile.height = tile_size;
    for (int row = 0; row < 2; row++) {
        int start_col = row == 0 ? 4 : 1;
        for (int col = start_col; col < 7; col++) {
            if (row == 1 && (col == 3 || col == 5)) {
                continue;
            }
            Rectangle src_rect = {TILE_SIZE * col, TILE_SIZE * row, TILE_SIZE, TILE_SIZE};
            DrawTexturePro(graphics_data.characters_texture, src_rect, tile, (Vector2){0, 0}, 0, WHITE);
            int this_char_index = row * 7 + col;
            if (outline_button(tile, OUTLINE_COLOR, 5.0, editor_selection == EDITOR_CHARACTER_SELECTED && editor_character_selected_index == this_char_index)) {
                if (editor_character_selected_index == this_char_index) {
                    editor_selection = EDITOR_NONE;
                } else {
                    editor_selection = EDITOR_CHARACTER_SELECTED;
                    editor_character_selected_index = this_char_index;
                }
            }
            tile.x += tile_size;
        }
        tile.y += tile_size;
        tile.x = panel.x;
    }
    tile_size = orig_size;
    tile.width = tile_size;
    tile.height = tile_size;

    // Draw items
    tile.x = panel.x;
    tile.y += tile_size;
    draw_item(tile, TILE_DOOR_1);
    tile.x += tile_size;
    draw_item(tile, TILE_DOOR_2);
    tile.x += tile_size;
    draw_item(tile, TILE_SIDE_DOOR_1);
    tile.x += tile_size;
    draw_item(tile, TILE_COIN_1);




    // Draw selected tile
    // if (editor->selected_tile != -1) {
    //     Rectangle selected_tile = {panel.x + panel.width / 2 - tile_size, panel.y + tile_size * (n_rows + 3 + 4), tile_size * 2, tile_size * 2};
    //     DrawTexturePro(graphics_data.tileset_texture, (Rectangle){TILE_SIZE * (editor->selected_tile % n_cols), TILE_SIZE * (editor->selected_tile / n_cols), TILE_SIZE, TILE_SIZE}, selected_tile, (Vector2){0, 0}, 0, WHITE);
    //     // If click on selected tile, deselect it
    //     if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && CheckCollisionPointRec(GetMousePosition(), selected_tile)) {
    //         editor->selected_tile = -1;
    //     }
    // }
    // if (editor_selection == EDITOR_CHARACTER_SELECTED) {
    //     Rectangle selected_character = {panel.x + panel.width / 2 - tile_size, panel.y + tile_size * (n_rows + 3 + 4), tile_size * 2, tile_size * 2};
    //     DrawTexturePro(graphics_data.characters_texture, (Rectangle){TILE_SIZE * (editor->selected_character % 7), TILE_SIZE * (editor->selected_character / 7), TILE_SIZE, TILE_SIZE}, selected_character, (Vector2){0, 0}, 0, WHITE);
    //     // If click on selected tile, deselect it
    //     if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && CheckCollisionPointRec(GetMousePosition(), selected_character)) {
    //     }
    // }


    // Draw button for viewing and loading saved levels
    float button_margin = 0.05 * panel.width;
    float button_width = 0.6 * panel.width;
    float button_height = button_width / 4.0;
    Rectangle load_level_button = {panel.x + button_margin, panel.y + panel.height - 4 * (button_height + button_margin), button_width, button_height};
    if (button(load_level_button, "Load level", BUTTON_COLOR, BUTTON_HOVER_COLOR, BUTTON_TEXT_COLOR)) {
        editor_load_level_open = true;
        reset_dir_listing();
    }



    // Draw button for saving
    Rectangle save_level_button = rect_below(load_level_button, button_margin);
    if (button(save_level_button, "Save level", BUTTON_COLOR, BUTTON_HOVER_COLOR, BUTTON_TEXT_COLOR)) {
        editor_save_level_open = true;
        reset_dir_listing();
    }


    Rectangle zoom_in_button = rect_below(save_level_button, button_margin);
    zoom_in_button.width /= 2;
    if (button(zoom_in_button, "+", BUTTON_COLOR, BUTTON_HOVER_COLOR, BUTTON_TEXT_COLOR)) {
        state->scale += 0.5f;
    }
    Rectangle zoom_out_button = rect_right(zoom_in_button, button_margin);
    if (button(zoom_out_button, "-", BUTTON_COLOR, BUTTON_HOVER_COLOR, BUTTON_TEXT_COLOR)) {
        state->scale -= 0.5f;
    }
}

void draw_edited_room(Rectangle panel, Editor *editor, float scale) {
    // Draw tiles
    for (int i = 0; i < editor->n_tiles; i++) {
        int tile = editor->tile_placements[i][0];
        int x = editor->tile_placements[i][1] + editor_x_offset;
        int y = editor->tile_placements[i][2] + editor_y_offset;
        if (tile == -1) {
            break;
        }
        Rectangle room_src = {TILE_SIZE * (tile % 10), TILE_SIZE * (tile / 10), TILE_SIZE, TILE_SIZE};
        Rectangle room_dest = {panel.x + x * TILE_SIZE * scale,
                               panel.y + y * TILE_SIZE * scale,
                               TILE_SIZE * scale,
                               TILE_SIZE * scale};
        DrawTexturePro(graphics_data.tileset_texture, room_src, room_dest, (Vector2){0, 0}, 0, WHITE);
    }

    // Draw collision boxes
    for (int i = 0; i < editor->n_collision_boxes; i++) {
        Rectangle collision_rect;
        Vector2 collision_box = editor->collision_boxes[i];
        collision_rect.x = panel.x + (collision_box.x + editor_x_offset) * scale * TILE_SIZE;
        collision_rect.y = panel.y + (collision_box.y + editor_y_offset) * scale * TILE_SIZE;
        collision_rect.width = TILE_SIZE * scale;
        collision_rect.height = TILE_SIZE * scale;
        DrawRectangleLinesEx(collision_rect, 1.0f, (Color){255, 0, 0, 255});
    }

    // Draw items
    for (int i = 0; i < editor->n_items; i++) {
        Rectangle item_rect;
        int item_id = editor->items[i][0];
        int x = editor->items[i][1] + editor_x_offset;
        int y = editor->items[i][2] + editor_y_offset;
        if (item_id == -1) {
            break;
        }
        item_rect.x = panel.x + x * scale * TILE_SIZE;
        item_rect.y = panel.y + y * scale * TILE_SIZE;
        item_rect.width = TILE_SIZE * scale;
        item_rect.height = TILE_SIZE * scale;
        DrawTexturePro(graphics_data.tileset_texture, (Rectangle){TILE_SIZE * (item_id % 10), TILE_SIZE * (item_id / 10), TILE_SIZE, TILE_SIZE}, item_rect, (Vector2){0, 0}, 0, WHITE);
    }

    // Draw characters
    for (int i = 0; i < editor->n_characters; i++) {
        Rectangle character_rect;
        int character_id = editor->placed_characters[i][0];
        int x = editor->placed_characters[i][1] + editor_x_offset;
        int y = editor->placed_characters[i][2] + editor_y_offset;
        if (character_id == -1) {
            break;
        }
        character_rect.x = panel.x + x * scale * TILE_SIZE;
        character_rect.y = panel.y + y * scale * TILE_SIZE;
        character_rect.width = TILE_SIZE * scale;
        character_rect.height = TILE_SIZE * scale;
        DrawTexturePro(graphics_data.characters_texture, (Rectangle){TILE_SIZE * (character_id % 7), TILE_SIZE * (character_id / 7), TILE_SIZE, TILE_SIZE}, character_rect, (Vector2){0, 0}, 0, WHITE);
    }


    // Draw starting point
    if (editor->starting_point_placed) {
        int x = editor->starting_point.x + editor_x_offset;
        int y = editor->starting_point.y + editor_y_offset;
        Rectangle starting_point = {panel.x + x * scale * TILE_SIZE, panel.y + y * scale * TILE_SIZE, TILE_SIZE * scale, TILE_SIZE * scale};
        DrawCircleLines(starting_point.x + starting_point.width / 2.0f, starting_point.y + starting_point.height / 2.0f, starting_point.width / 2.0f, (Color){0, 255, 0, 255});
    }

    // Mouse click handler
    bool left_pressed = IsMouseButtonReleased(MOUSE_LEFT_BUTTON);
    bool ctrl_pressed = IsKeyDown(KEY_LEFT_CONTROL);
    Vector2 mouse_pos = GetMousePosition();
    if (left_pressed && CheckCollisionPointRec(mouse_pos, panel)) {
        int x = (mouse_pos.x - panel.x) / (TILE_SIZE * scale);
        int y = (mouse_pos.y - panel.y) / (TILE_SIZE * scale);
        x -= editor_x_offset;
        y -= editor_y_offset;

        // TILE PLACEMENT
        if (tile_is_selected() && !rectangle_drawing_active) {
             if (ctrl_pressed) {
                for (int i = editor->n_tiles - 1; i >= 0; i--) {
                    if (editor->tile_placements[i][1] == x && editor->tile_placements[i][2] == y) {
                        for (int j = i; j < editor->n_tiles - 1; j++) {
                            editor->tile_placements[j][0] = editor->tile_placements[j + 1][0];
                            editor->tile_placements[j][1] = editor->tile_placements[j + 1][1];
                            editor->tile_placements[j][2] = editor->tile_placements[j + 1][2];
                        }
                        editor->n_tiles--;
                        break;
                    }
                }
            } else {
                // Select random left wall
                int index = get_tile_index();
                editor->tile_placements[editor->n_tiles][0] = index;
                editor->tile_placements[editor->n_tiles][1] = x;
                editor->tile_placements[editor->n_tiles][2] = y;
                editor->n_tiles++;
            }
        }

        // ITEM PLACEMENT
        if (editor_selection == EDITOR_ITEM_SELECTED) {
            // Check if there already is an item 
            bool item_exists = false;
            for (int i = 0; i < editor->n_items; i++) {
                if (editor->items[i][1] == x && editor->items[i][2] == y) {
                    if (ctrl_pressed) {
                        for (int j = i; j < editor->n_items - 1; j++) {
                            editor->items[j][0] = editor->items[j + 1][0];
                            editor->items[j][1] = editor->items[j + 1][1];
                            editor->items[j][2] = editor->items[j + 1][2];
                        }
                        editor->n_items--;
                    }
                    item_exists = true;
                    break;
                }
            }

            if (!item_exists) {
                editor->items[editor->n_items][0] = editor_item_selected_index;
                editor->items[editor->n_items][1] = x;
                editor->items[editor->n_items][2] = y;
                editor->n_items++;
            }
        }

        // COLLISION BOX PLACEMENT
        if (editor_selection == EDITOR_COLLISION_BOX_SELECTED) {
            // Check if there already is a collision box, if there is, remove it
            bool collision_box_exists = false;
            for (int i = 0; i < editor->n_collision_boxes; i++) {
                if (editor->collision_boxes[i].x == x && editor->collision_boxes[i].y == y) {
                    for (int j = i; j < editor->n_collision_boxes - 1; j++) {
                        editor->collision_boxes[j] = editor->collision_boxes[j + 1];
                    }
                    editor->n_collision_boxes--;
                    collision_box_exists = true;
                    break;
                }
            }


            if (!collision_box_exists) {
                editor->collision_boxes[editor->n_collision_boxes].x = x;
                editor->collision_boxes[editor->n_collision_boxes].y = y;
                editor->n_collision_boxes++;
            }
        }

        if (editor_selection == EDITOR_STARTING_POINT_SELECTED) {
            if (editor->starting_point_placed) {
                editor->starting_point_placed = false;
            } else {
                editor->starting_point.x = x;
                editor->starting_point.y = y;
                editor->starting_point_placed = true;
            }
        }

        // CHARACTER PLACEMENT
        if (editor_selection == EDITOR_CHARACTER_SELECTED) {
            // If there already is a character, remove it
            bool character_exists = false;
            for (int i = 0; i < editor->n_characters; i++) {
                if (editor->placed_characters[i][1] == x && editor->placed_characters[i][2] == y) {
                    for (int j = i; j < editor->n_characters - 1; j++) {
                        editor->placed_characters[j][0] = editor->placed_characters[j + 1][0];
                        editor->placed_characters[j][1] = editor->placed_characters[j + 1][1];
                        editor->placed_characters[j][2] = editor->placed_characters[j + 1][2];
                    }
                    editor->n_characters--;
                    character_exists = true;
                    break;
                }
            }

            if (!character_exists) {
                editor->placed_characters[editor->n_characters][0] = editor_character_selected_index;
                editor->placed_characters[editor->n_characters][1] = x;
                editor->placed_characters[editor->n_characters][2] = y;
                editor->n_characters++;
            }
        }
    }

    // Rectangle draw tool
    bool shift_pressed = IsKeyDown(KEY_LEFT_SHIFT);
    bool left_down = IsMouseButtonDown(MOUSE_LEFT_BUTTON);
    if (left_down && shift_pressed && CheckCollisionPointRec(GetMousePosition(), panel)) {
        int x = (GetMouseX() - panel.x) / (TILE_SIZE * scale);
        int y = (GetMouseY() - panel.y) / (TILE_SIZE * scale);
        if (tile_is_selected()) {
            // Rectangle drawing mode
            if (!rectangle_drawing_active) {
                rectangle_start = (Vector2){x, y};
                rectangle_drawing_active = true;
            }

            rectangle_end = (Vector2){x, y};

            int x1 = panel.x + rectangle_start.x * TILE_SIZE * scale;
            int y1 = panel.y + rectangle_start.y * TILE_SIZE * scale;
            int x2 = panel.x + rectangle_end.x * TILE_SIZE * scale;
            int y2 = panel.y + rectangle_end.y * TILE_SIZE * scale;

            if (x1 > x2) {
                int temp = x1;
                x1 = x2;
                x2 = temp;
            }
            if (y1 > y2) {
                int temp = y1;
                y1 = y2;
                y2 = temp;
            }
            DrawRectangleLines(x1, y1, x2 - x1 + TILE_SIZE * scale, y2 - y1 + TILE_SIZE * scale, RED);
        }
    }
    if (!left_down) {
        if (rectangle_drawing_active) {
            // Fill squares in rectangle
            int x1 = rectangle_start.x;
            int y1 = rectangle_start.y;
            int x2 = rectangle_end.x;
            int y2 = rectangle_end.y;
            if (x1 > x2) {
                int temp = x1;
                x1 = x2;
                x2 = temp;
            }
            if (y1 > y2) {
                int temp = y1;
                y1 = y2;
                y2 = temp;
            }

            switch (editor_selection) {
                case EDITOR_FLOOR_SELECTED:
                    {
                        for (int y = y1; y <= y2; y++) {
                            for (int x = x1; x <= x2; x++) {
                                int tile_index = get_tile_index();
                                editor->tile_placements[editor->n_tiles][0] = tile_index;
                                editor->tile_placements[editor->n_tiles][1] = x;
                                editor->tile_placements[editor->n_tiles][2] = y;
                                editor->n_tiles++;
                            }
                        }
                        break;
                    }
                case EDITOR_LEFT_WALL_SELECTED:
                case EDITOR_TOP_WALL_SELECTED:
                case EDITOR_RIGHT_WALL_SELECTED:
                case EDITOR_BOTTOM_WALL_SELECTED:
                case EDITOR_BOTTOM_LEFT_CORNER_SELECTED: 
                case EDITOR_BOTTOM_RIGHT_CORNER_SELECTED:
                    {
                        EditorSelection original = editor_selection;

                        // Draw floor tile
                        editor_selection = EDITOR_FLOOR_SELECTED;
                        for (int y = y1; y <= y2; y++) {
                            for (int x = x1; x <= x2; x++) {
                                int tile_index = get_tile_index();
                                editor->tile_placements[editor->n_tiles][0] = tile_index;
                                editor->tile_placements[editor->n_tiles][1] = x;
                                editor->tile_placements[editor->n_tiles][2] = y;
                                editor->n_tiles++;
                            }
                        }


                        // Draw only walls
                        editor_selection = EDITOR_LEFT_WALL_SELECTED;
                        for (int y = y1; y <= y2; y++) {
                            place_wall(x1, y, get_tile_index(), editor);
                        }

                        editor_selection = EDITOR_TOP_WALL_SELECTED;
                        for (int x = x1 + 1; x < x2; x++) {
                            place_wall(x, y1, get_tile_index(), editor);
                        }

                        editor_selection = EDITOR_RIGHT_WALL_SELECTED;
                        for (int y = y1; y <= y2; y++) {
                            place_wall(x2, y, get_tile_index(), editor);
                        }

                        editor_selection = EDITOR_BOTTOM_LEFT_CORNER_SELECTED;
                        place_wall(x1, y2, get_tile_index(), editor);

                        editor_selection = EDITOR_BOTTOM_WALL_SELECTED;
                        for (int x = x1 + 1; x < x2; x++) {
                            place_wall(x, y2, get_tile_index(), editor);
                        }

                        editor_selection = EDITOR_BOTTOM_RIGHT_CORNER_SELECTED;
                        place_wall(x2, y2, get_tile_index(), editor);

                        editor_selection = original;
                    }
                default:
                    break;
            }
        }
        rectangle_drawing_active = false;
    }

    // Paint handler
    bool right_down = IsMouseButtonDown(MOUSE_RIGHT_BUTTON);
    if (right_down && CheckCollisionPointRec(GetMousePosition(), panel)) {
        int x = (GetMouseX() - panel.x) / (TILE_SIZE * scale);
        int y = (GetMouseY() - panel.y) / (TILE_SIZE * scale);
        // if (editor->selected_tile != -1) {
        if (tile_is_selected()) { // TODO Function for determining whether a tile is selected
            // Check for existing tile, and remove it
            bool tile_exists = false;
            for (int i = 0; i < editor->n_tiles; i++) {
                if (editor->tile_placements[i][1] == x && editor->tile_placements[i][2] == y) {
                    tile_exists = true;
                    break;
                }
            }

            if (!tile_exists) {
                int index = get_tile_index();
                editor->tile_placements[editor->n_tiles][0] = index;
                // editor->tile_placements[editor->n_tiles][0] = editor->selected_tile;
                editor->tile_placements[editor->n_tiles][1] = x;
                editor->tile_placements[editor->n_tiles][2] = y;
                editor->n_tiles++;
            } 
        }

        if (editor_selection == EDITOR_COLLISION_BOX_SELECTED) {
            // Check if there already is a collision box, if there is, don't add a new one
            bool collision_box_exists = false;
            for (int i = 0; i < editor->n_collision_boxes; i++) {
                if (editor->collision_boxes[i].x == x && editor->collision_boxes[i].y == y) {
                    collision_box_exists = true;
                    break;
                }
            }

            if (!collision_box_exists) {
                editor->collision_boxes[editor->n_collision_boxes].x = x;
                editor->collision_boxes[editor->n_collision_boxes].y = y;
                editor->n_collision_boxes++;
            }
        }
    }


    // Draw hover box
    if (CheckCollisionPointRec(GetMousePosition(), panel)) {
        int x = (GetMouseX() - panel.x) / (TILE_SIZE * scale);
        int y = (GetMouseY() - panel.y) / (TILE_SIZE * scale);
        DrawRectangleLinesEx((Rectangle){panel.x + x * TILE_SIZE * scale, panel.y + y * TILE_SIZE * scale, TILE_SIZE * scale, TILE_SIZE * scale}, 1, RED);
    }
}


void clear_editor(Editor *editor) {
    for (int i = 0; i < TILE_EDITOR_CAPACITY; i++) {
        editor->tile_placements[i][0] = -1;
    }
    editor->n_tiles = 0;

    for (int i = 0; i < TILE_EDITOR_CAPACITY; i++) {
        editor->collision_boxes[i].x = -1;
    }
    editor->n_collision_boxes = 0;


    // Clear characters
    for (int i = 0; i < TILE_EDITOR_CAPACITY; i++) {
        editor->placed_characters[i][0] = -1;
    }
    editor->n_characters = 0;

    editor_selection = EDITOR_NONE;

    // Clear items
    for (int i = 0; i < TILE_EDITOR_CAPACITY; i++) {
        editor->items[i][0] = -1;
    }
    editor->n_items = 0;
}


LevelData create_level_data(const Editor *editor) {
    LevelData level_data = {0};
    level_data.n_tiles = editor->n_tiles;
    level_data.starting_point = editor->starting_point;
    level_data.tile_placements = malloc(sizeof(int) * editor->n_tiles * 3);


    for (int i = 0; i < editor->n_tiles; i++) {
        level_data.tile_placements[i * 3 + 0] = editor->tile_placements[i][0];
        level_data.tile_placements[i * 3 + 1] = editor->tile_placements[i][1];
        level_data.tile_placements[i * 3 + 2] = editor->tile_placements[i][2];
    }


    // COLLISION STUFF
    level_data.n_collision_boxes = editor->n_collision_boxes + editor->n_items;
    level_data.collision_boxes = malloc(sizeof(CollisionBox) * level_data.n_collision_boxes);
    for (int i = 0; i < editor->n_collision_boxes; i++) {
        level_data.collision_boxes[i].position = editor->collision_boxes[i];
        level_data.collision_boxes[i].is_active = true;
    }
    int offset = editor->n_collision_boxes;
    for (int i = 0; i < editor->n_items; i++) {
        level_data.collision_boxes[i + offset].position = (Vector2){editor->items[i][1], editor->items[i][2]};
        level_data.collision_boxes[i + offset].is_active = false;
    }

    level_data.entity_count = editor->n_characters;
    level_data.entities = malloc(sizeof(Entity) * editor->n_characters);
    for (int i = 0; i < editor->n_characters; i++) {
        level_data.entities[i] = create_entity_from_id(editor->placed_characters[i][0], editor->placed_characters[i][1], editor->placed_characters[i][2]);
    }


    // Transfer items
    int collision_offset = editor->n_collision_boxes;
    level_data.n_items = editor->n_items;
    printf("n_items: %d\n", level_data.n_items);
    level_data.items = malloc(sizeof(Item) * editor->n_items);
    for (int i = 0; i < editor->n_items; i++) {
        int main_id = editor->items[i][0];
        int x = editor->items[i][1];
        int y = editor->items[i][2];
        CollisionBox *collision_box_ptr = &level_data.collision_boxes[i + collision_offset];
        Item item = setup_item(main_id, collision_box_ptr, x, y);
        level_data.items[i] = item;
    }


    // Initialize summons
    level_data.summons_count = 0;
    level_data.summons_capacity = 128;
    level_data.summons = malloc(sizeof(Entity) * level_data.summons_capacity);

    level_data.num_dead_summons = 0;

    return level_data;
}


void editor(GameState *state) {
    Rectangle window = state->window;
    Rectangle side_panel, main_panel;
    split_rectangle_at(window, &side_panel, &main_panel, 0.25f, true);
    ClearBackground(BLACK);


    if (editor_save_level_open) {
        // List all current exising files so we can choose a unique name
        // TODO: Save the filenames for final check that we dont overwrite
        // Make a text box where the file name can be put in
        // Select filename for save file and save current editor
        if (!dir_already_read) {
            populate_dir_listing();
            dir_already_read = true;
        }

        int draw_offset_x = 10;
        int draw_offset_y = 10;
        int font_size = 40;
        for (int i = 0; i < n_files; i++) {
            if (draw_offset_y + font_size > 0.8 * window.height) {
                draw_offset_y = 10;
                draw_offset_x += 300;
            }
            char *name = files[i];

            Rectangle click_rect = {draw_offset_x, draw_offset_y, 300, font_size};
            if (button(click_rect, name, BUTTON_COLOR, BUTTON_HOVER_COLOR, BUTTON_TEXT_COLOR)) {
                sprintf(input_field, "%s", name); 
                input_ptr = strlen(input_field);
            }
            draw_offset_y += font_size * 1.5;
        }

        font_size /= 2;
        Rectangle input_rect = {10, 0.8 * window.height + font_size, 600, font_size * 1.2};
        DrawRectangleRec(input_rect, WHITE);
        DrawText(input_field, input_rect.x + 10, input_rect.y + font_size * 0.1, font_size, BLACK);

        int key_code = GetKeyPressed();
        if (key_code != 0) {
            if (key_code == KEY_BACKSPACE) {
                if (input_ptr > 0) {
                    input_ptr--;
                    input_field[input_ptr] = '\0';
                }
            } else if (input_ptr < INPUT_CAP - 1) {
                input_field[input_ptr] = key_code;
                input_ptr++;
            }
        }

        font_size *= 2;
        const char* save_text = "Save";
        int text_length = MeasureText(save_text, font_size);
        Rectangle save_button = {10, 0.8 * window.height + font_size * 2, text_length + 20, font_size * 1.2};

        if (button(save_button, "Save", BUTTON_COLOR, BUTTON_HOVER_COLOR, BUTTON_TEXT_COLOR)) {
            if (strlen(input_field) > 0) {
                sprintf(temp_char, "levels/%s", input_field);
                editor_save(temp_char, &state->editor);
                editor_save_level_open = false;
            }
        }

        Rectangle cancel_button = rect_right(save_button, 20);
        if (button(cancel_button, "Cancel", BUTTON_COLOR, BUTTON_HOVER_COLOR, BUTTON_TEXT_COLOR)) {
            editor_save_level_open = false;
        }
    } else if (editor_load_level_open) {
        // Show available saves and allow loading one on click
        if (!dir_already_read) {
            populate_dir_listing();
            dir_already_read = true;
        }

        int draw_offset_x = 10;
        int draw_offset_y = 10;
        int font_size = 40;
        for (int i = 0; i < n_files; i++) {
            if (draw_offset_y + font_size > 0.8 * window.height) {
                draw_offset_y = 10;
                draw_offset_x += 300;
            }
            char *name = files[i];

            Rectangle click_rect = {draw_offset_x, draw_offset_y, 300, font_size};
            if (button(click_rect, name, BUTTON_COLOR, BUTTON_HOVER_COLOR, BUTTON_TEXT_COLOR)) {
                sprintf(input_field, "%s", name); 
                input_ptr = strlen(input_field);
            }
            draw_offset_y += font_size * 1.5;
        }

        font_size /= 2;
        Rectangle input_rect = {10, 0.8 * window.height + font_size, 600, font_size * 1.2};
        DrawRectangleRec(input_rect, WHITE);
        DrawText(input_field, input_rect.x + 10, input_rect.y + font_size * 0.1, font_size, BLACK);

        Rectangle load_button = rect_bottom_left(window, window.width * 0.2, 40, 20, 20);
        if (button(load_button, "Load level", BUTTON_COLOR, BUTTON_HOVER_COLOR, BUTTON_TEXT_COLOR)) {
            if (strlen(input_field) > 0) {
                sprintf(temp_char, "levels/%s", input_field);
                editor_load(temp_char, &state->editor);
                editor_load_level_open = false;
            }
        }
        
        Rectangle cancel_button = rect_right(load_button, 20);
        if (button(cancel_button, "Cancel", BUTTON_COLOR, BUTTON_HOVER_COLOR, BUTTON_TEXT_COLOR)) {
            editor_load_level_open = false;
        }
    } else {
        draw_tile_selector(side_panel, &state->editor, state);
        DrawRectangleRec(main_panel, (Color){0, 0, 51, 255});

        BeginScissorMode(main_panel.x, main_panel.y, main_panel.width, main_panel.height);
        draw_edited_room(main_panel, &state->editor, state->scale);
        EndScissorMode();

        if (IsKeyReleased(KEY_R)) {
            clear_editor(&state->editor);
        }
    
        if (IsKeyReleased(KEY_E)) {
            state->editor_enabled = false;
        }
    
        if (IsKeyReleased(KEY_T)) {
            // Create LevelData from editor data
            LevelData level_data = create_level_data(&state->editor);
            // Free old level data
            free_level_data(state->level);
            // Set level data to new level data
            state->level = level_data;
        }
    
        if (IsKeyReleased(KEY_D)) {
            // Dump info about editor, skip arrays
            printf("Editor:\n");
            printf("  n_tiles: %d\n", state->editor.n_tiles);
            printf("  n_collision_boxes: %d\n", state->editor.n_collision_boxes);
            printf("  starting_point: (%f, %f)\n", state->editor.starting_point.x, state->editor.starting_point.y);
            printf("  selection: %d\n", editor_selection);
        }
    
        if (IsKeyReleased(KEY_O)) {
            editor_save("test.txt", &state->editor);
        }
    
        if (IsKeyReleased(KEY_I)) {
            Editor new_editor;
            editor_load("test.txt", &new_editor);
            printf("new ntiles: %d\n", new_editor.n_tiles);
            for (int i = 0; i < 5; i++) {
                printf("New tile id: %d, x: %d, y: %d\n", new_editor.tile_placements[i][0], new_editor.tile_placements[i][1], new_editor.tile_placements[i][2]);
            }
        }

        if (IsKeyReleased(KEY_P)) {
            TakeScreenshot("screenshot.png");
        }

        if (IsKeyPressed(KEY_LEFT)) {
            editor_x_offset -= 1;
        }
        if (IsKeyPressed(KEY_RIGHT)) {
            editor_x_offset += 1;
        }
        if (IsKeyPressed(KEY_UP)) {
            editor_y_offset -= 1;
        }
        if (IsKeyPressed(KEY_DOWN)) {
            editor_y_offset += 1;
        }
    }
}

#define DEF_RW_SINGLE(type) \
    void write_##type(type *value, FILE *file) { \
        fwrite(value, sizeof(type), 1, file); \
    } \
    void read_##type(type *dest, FILE *file) { \
        fread(dest, sizeof(type), 1, file); \
    }

#define DEF_RW_MANY(type) \
    void write_##type##s(type *values, int n_values, FILE *file) { \
        fwrite(values, n_values, sizeof(type), file); \
    } \
    void read_##type##s(type *dest, int n_values, FILE *file) { \
        fread(dest, n_values, sizeof(type), file); \
    }

DEF_RW_SINGLE(int)
DEF_RW_SINGLE(bool)
DEF_RW_MANY(int)
DEF_RW_SINGLE(Vector2)
DEF_RW_MANY(Vector2)

#define steps_for(rorw) \
    rorw##_int(&editor->n_tiles, save_file); \
    rorw##_ints(&editor->tile_placements[0][0], TILE_EDITOR_CAPACITY * 3, save_file); \
\
    rorw##_Vector2s(editor->collision_boxes, TILE_EDITOR_CAPACITY, save_file); \
    rorw##_int(&editor->n_collision_boxes, save_file); \
\
\
    rorw##_Vector2(&editor->starting_point, save_file); \
    rorw##_bool(&editor->starting_point_placed, save_file); \
\
\
    rorw##_ints(&editor->placed_characters[0][0], TILE_EDITOR_CAPACITY * 3, save_file); \
    rorw##_int(&editor->n_characters, save_file); \
\
\
    rorw##_ints(&editor->items[0][0], TILE_EDITOR_CAPACITY * 3, save_file); \
    rorw##_int(&editor->n_items, save_file); \



void editor_save(const char *filename, Editor *editor) {
    FILE *save_file = fopen(filename, "w");
    steps_for(write);
    fclose(save_file);
    printf("Saved\n");
}

void editor_load(const char *filename, Editor *editor) {
    FILE *save_file = fopen(filename, "r");
    steps_for(read);
    fclose(save_file);
    printf("Loaded\n");
}

