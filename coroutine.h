#ifndef COROUTINE_H


typedef struct
{
	float elapsed;
	int index;
} coroutine_t;

static inline void co_init(coroutine_t* co)
{
	co->elapsed = 0;
	co->index = 0;
}

#define CO_START(co)          do { switch (co->index) { default: /* default is 0, and will run just once. */

#define CO_CASE(co, name)     case __LINE__: name: co->index = __LINE__;

#define CO_WAIT(co, time, dt) do { case __LINE__: co->index = __LINE__; co->elapsed += dt; do { if (co->elapsed < time) { goto __co_end; } else { co->elapsed = 0; } } while (0); } while (0)

#define CO_WAIT_OFFSET(co, time, dt, offset) do { case __LINE__: co->index = __LINE__ - offset; co->elapsed += dt; do { if (co->elapsed < time) { goto __co_end; } else { co->elapsed = 0; } } while (0); } while (0)

#define CO_WAIT_CASE(co, time, dt, line) do { case __LINE__: co->index = line; co->elapsed += dt; do { if (co->elapsed < time) { goto __co_end; } else { co->elapsed = 0; } } while (0); } while (0)

#define CO_EXIT(co)           do { goto __co_end; } while (0)

#define CO_YIELD(co)          do { co->index = __LINE__; COROUTINE_EXIT(co); case __LINE__:; } while (0)

#define CO_END(co)            } co->index = 0; co->elapsed = 0.0; __co_end:; } while (0)

#define COROUTINE_H
#endif // COROUTINE_H

