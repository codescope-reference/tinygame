#ifndef EDITOR_STRUCT_H
#define EDITOR_STRUCT_H
#include "raylib.h"

#define TILE_EDITOR_CAPACITY 512
typedef struct Editor {
    // Tiles
    int n_tiles;
    int tile_placements[TILE_EDITOR_CAPACITY][3];

    // Collision boxes
    Vector2 collision_boxes[TILE_EDITOR_CAPACITY];
    int n_collision_boxes;

    // Starting point
    Vector2 starting_point;
    bool starting_point_placed;

    // Characters
    int placed_characters[TILE_EDITOR_CAPACITY][3];
    int n_characters;


    // Items
    int items[TILE_EDITOR_CAPACITY][3];
    int n_items;
} Editor;
#endif
