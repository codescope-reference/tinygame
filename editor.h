#ifndef EDITOR_H
#define EDITOR_H
#include "button.c"
#include "rect_utils.c"
#include "game_state.h"
#include "level.h"
#include "editor_struct.h"
#include "tiles.h"



bool tile_is_selected(void);
int get_tile_index(void);
void editor_save(const char *filename, Editor * editor);
void editor_load(const char *filename, Editor *editor);
void reset_dir_listing(void);
void populate_dir_listing(void);
void place_wall(int x, int y, int tile_index, Editor *editor);
void split_rectangle_at(Rectangle src, Rectangle *panel1, Rectangle *panel2, float at, bool vertical_split);
void draw_collision_box_selector(Rectangle rect, Editor *editor);
void draw_starting_point_selector(Rectangle rect, Editor *editor);
bool item_ids_contains(int value);
void draw_item(Rectangle location, int item_index);
void draw_tile_selector(Rectangle panel, Editor *editor, GameState *state);
void draw_edited_room(Rectangle panel, Editor *editor, float scale) ;
void clear_editor(Editor *editor);
LevelData create_level_data(const Editor *editor);
void editor(GameState *state);

#endif

