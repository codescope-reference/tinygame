#ifndef LEVEL_H
#define LEVEL_H
#include "entity.h"
#include "item.h"
#include "collision_box.h"




typedef struct LevelData {
    int *tile_placements;
    int n_tiles;

    CollisionBox *collision_boxes;
    int n_collision_boxes;

    Vector2 starting_point;

    Entity *entities; // TODO: Rename to enemies
    int entity_count;
    unsigned int num_dead; // TODO: Rename to num_dead_enemies


    Entity *summons;
    int summons_count;
    int summons_capacity;
    unsigned int num_dead_summons;



    // ITEMS
    Item *items;
    int n_items;


    // ----
    bool debug_mode;

} LevelData;


void free_level_data(LevelData level_data);
#endif
