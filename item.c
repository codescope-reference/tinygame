#include "item.h"


void item_update(Item *item) {
    static_assert(ITEM_KIND_COUNT == 2, "Update item_update function");
    switch (item->item_kind) {
        case ITEM_DOOR:
            if (IsKeyReleased(KEY_F)) {
                if (item->active_version == item->tile_id_1) {
                    item->active_version = item->tile_id_2;
                    item->collision_box->is_active = false;
                } else {
                    item->active_version = item->tile_id_1;
                    item->collision_box->is_active = true;
                }
            }
            break;
        case ITEM_COIN:
            if (item->active_version != -1) {
                game_state.player.summons_capacity++;
                item->active_version = -1;
            }
            break;
        default:
            break;
    }
}




Item setup_item(int main_id, CollisionBox *collision_box, int x, int y) {
    int tile_id_2;
    ItemKind kind;
    bool collision_active = true;

    switch (main_id) {
        case TILE_DOOR_1:
        case TILE_DOOR_2:
            tile_id_2 = main_id + 11;
            kind = ITEM_DOOR;
            break;
        case TILE_SIDE_DOOR_1:
            tile_id_2 = -1;
            kind = ITEM_DOOR;
            break;
        case TILE_COIN_1:
            tile_id_2 = -1;
            kind = ITEM_COIN;
            collision_active = false;
            break;
        default:
            tile_id_2 = -1;
            break;
    }
    collision_box->is_active = collision_active;

    Item item = {
       .tile_id_1 = main_id,
       .tile_id_2 = tile_id_2,
       .active_version = main_id,
       .x = x,
       .y = y,
       .item_kind = kind,
       .collision_box = collision_box,
    };

    return item;
}
