#ifndef SPRITE_H
#define SPRITE_H
typedef struct Sprite {
    Image *images;
    Texture2D *textures;
    int frame_count;
    int frames_per_frame;
    bool flip_x; // TODO: Move this to the entity
} Sprite;


Sprite *load_sprite_char(const char *name);
Sprite *load_sprite(const char **name, int n_frames, int frames_per_frame);
void animate(Sprite *sprite, int frame_count, Vector2 position, float scale);
void free_sprite(Sprite *sprite);


#endif
