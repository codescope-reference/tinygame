/*
To use this file first #define LLISTNAME and LLISTVAL.
LLISTNAME will be the name of the linked list type and will also
post-fix all function names, e.g. #define LLISTNAME MyList -> createMyList/freeMyList/etc.

VAL will be the type of the data contained in the linked list, e.g. 
#define LLISTTYPE int -> MyList.value will be an int.

If you redefine LLISTNAME and LLISTTYPE and re-include this file,
you can have multiple versions of a linked list, thus providing some form
of generics in C.
 */


#define FNAME(x, y) concat(x, y)
#define concat(x, y) x ## y

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>


typedef struct LLISTNAME {
  LLISTTYPE value;
  struct LLISTNAME *next;
} LLISTNAME;


LLISTNAME *FNAME(create, LLISTNAME)(LLISTTYPE value) {
  LLISTNAME *ll = malloc(sizeof(LLISTNAME));
  ll->value = value;
  ll->next = NULL;
  
  return ll;
}

void FNAME(free, LLISTNAME)(LLISTNAME *node) {
  assert(node != NULL);
  if (node->next != NULL) {
    FNAME(free, LLISTNAME)(node->next);
  }
  free(node);
}


void FNAME(freeLarge, LLISTNAME)(LLISTNAME *node) {
  assert(node != NULL);
  LLISTNAME * head = node;
  while (head != NULL) {
    LLISTNAME *next = head->next;
    free(head);
    head = next;
  }
}

void FNAME(link, LLISTNAME)(LLISTNAME *n1, LLISTNAME *n2) {
  assert(n1->next == NULL);
  n1->next = n2;
}

LLISTNAME *FNAME(getNth, LLISTNAME)(LLISTNAME *start, int n) {
  assert(n >= 0);
  if (n == 0) {
    return start;
  }
  LLISTNAME *result = start;
  for (int i = n; i > 0; i--) {
    assert(result->next != NULL);
    result = result->next;
  }

  return result;
}


int FNAME(length, LLISTNAME)(LLISTNAME *start) {
  int l = 0;
  LLISTNAME *current = start;
  while (current != NULL) {
    l++;
    current = current->next;
  }
  return l;
}

LLISTNAME * FNAME(cons, LLISTNAME)(LLISTNAME * start, LLISTTYPE value) {
  LLISTNAME * newStart = FNAME(create, LLISTNAME)(value);
  newStart->next = start;
  return newStart;
}


void FNAME(freeNode, LLISTNAME)(LLISTNAME * node) {
  // Free only this node and not any other nodes the node points to
  assert(node != NULL);
  free(node);
}

