#ifndef ENTITY_H
#define ENTITY_H
#include "sprite.h"
#include "attack_dict.h"


enum Attacks {
    ATTACK_PLAYER_AXE,
    ATTACK_PLAYER_SWORD,
    ATTACK_PLAYER_RANGED,
    ATTACK_COUNT,
};


// A single coroutine struct cannot participate in multiple things at once (e.g. animation and a timer)
// Therefore each entity will need multiple coroutines
enum CoTypes {
    CO_ANIMATE = 0,
    CO_TIMER,

    CO_COUNT,
};

typedef struct Entity {
    // Graphical data
    Sprite *sprite;
    Vector2 position;
    Vector2 velocity;

    // Gameplay data
    int health;
    bool is_dead;
    bool is_attacking;
    bool is_invincible;
    // We register every time an attack is made to this entity,
    // who made the attack, the damage, and when it was made.
    // That way we can rate limit attacks on a per entity basis.
    // We later apply damage and remove registered attacks after a delay
    EntityIDAtkDict *attacks;

    Rectangle collision_box;



    EntityID id;


    coroutine_t co[CO_COUNT];
} Entity;

Entity create_entity(Sprite *sprite, Vector2 position, int health, Rectangle collision_box);
void free_entity(Entity *entity);
Entity create_skeleton1(Vector2 position);
void register_attack_on(Entity *target, Entity *attacker, int damage);
Entity create_entity_from_id(int id, int x, int y);
EntityID get_next_id(void);
#endif
