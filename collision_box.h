#ifndef COLLISION_BOX_H
#define COLLISION_BOX_H
typedef struct CollisionBox {
    Vector2 position;
    bool is_active;
} CollisionBox;
#endif
