void free_level_data(LevelData level_data) {
    if (level_data.tile_placements) {
        free(level_data.tile_placements);
    }
    if (level_data.collision_boxes) {
        free(level_data.collision_boxes);
    }
    if (level_data.entities) {
        free(level_data.entities);
    }
    if (level_data.items) {
        free(level_data.items);
    }
}
