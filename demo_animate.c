


void move_left_right(Entity *entity, float dt) {
    coroutine_t *co = &entity->co[CO_ANIMATE];

    float walk_time = 1.0;
    CO_START(co);
    CO_WAIT(co, 1, dt);
    entity->velocity.x = -5;
    CO_WAIT(co, walk_time, dt);
    entity->velocity.x = 0;
    CO_WAIT(co, 1, dt);
    entity->velocity.x = 5;
    CO_WAIT(co, walk_time, dt);
    entity->velocity.x = 0;
    CO_END(co);
}


void patrol(Entity *entity, float dt) {
    coroutine_t *co = &entity->co[CO_ANIMATE];

    float walk_time = 1.0f;
    float wait_time = 0.75f;
    CO_START(co);
    // Initial pause
    CO_WAIT(co, wait_time, dt);

    // Walk for some time
    entity->velocity.y = 5;
    CO_WAIT(co, walk_time, dt);

    // Stop for some time
    entity->velocity.y = 0;
    CO_WAIT(co, wait_time, dt);

    // Walk for some time
    entity->velocity.x = -5;
    CO_WAIT(co, walk_time, dt);

    // Stop for some time
    entity->velocity.x = 0;
    CO_WAIT(co, wait_time, dt);

    // Walk for some time
    entity->velocity.y = -5;
    CO_WAIT(co, walk_time, dt);

    // Stop for some time
    entity->velocity.y = 0;
    CO_WAIT(co, wait_time, dt);

    // Walk for some time
    entity->velocity.x = 5;
    CO_WAIT(co, walk_time, dt);

    // Stop for some time
    entity->velocity.x = 0;
    CO_END(co);
}


void draw_attack_at(const Vector2 offset, Sprite *sprite, int frame_number, Vector2 position, float scale) {
    DrawTexturePro(sprite->textures[frame_number],
                   (Rectangle){0, 0, TILE_SIZE, TILE_SIZE},
                   (Rectangle){(position.x + offset.x) * scale * TILE_SIZE, (position.y + offset.y) * scale * TILE_SIZE, TILE_SIZE * scale, TILE_SIZE * scale},
                   (Vector2){0, 0},
                   0,
                   WHITE);
}


void check_and_register_collisions(Entity *attacker,
                                   Vector2 attack_position,
                                   const LevelData *level,
                                   Rectangle attack_hit_box_original,
                                   float scale,
                                   Vector2 offset,
                                   bool debug_mode) {
    for (int i = 0; i < level->entity_count; i++) {
        Entity *entity = &level->entities[i];


        Rectangle attack_hit_box = attack_hit_box_original;
        attack_hit_box.x += attack_position.x;
        attack_hit_box.y += attack_position.y;
        Rectangle attack_collision_box = (Rectangle){attack_hit_box.x * scale * TILE_SIZE,
                                                     attack_hit_box.y * scale * TILE_SIZE,
                                                     TILE_SIZE * scale * attack_hit_box.width,
                                                     TILE_SIZE * scale * attack_hit_box.height};

        if (debug_mode) {
            // Draw a red box around the attack_collision_box
            Rectangle attack_box_draw = attack_collision_box;
            attack_box_draw.x += offset.x * scale * TILE_SIZE;
            attack_box_draw.y += offset.y * scale * TILE_SIZE;
            DrawRectangleLinesEx(attack_box_draw, 1, RED);
        }
        if (entity->is_dead) continue;

        if (CheckCollisionRecs(attack_collision_box,
                               (Rectangle){entity->position.x * scale * TILE_SIZE, entity->position.y * scale * TILE_SIZE, TILE_SIZE * scale, TILE_SIZE * scale})) {
            register_attack_on(entity, attacker, 5);  // TODO: Look up attack damage on player
            // entity->health -= 5; // TODO: Look up attack damage on player
        }
    }
}

#define ABOVE(entity) (Vector2){entity->position.x, entity->position.y - 1.0f}
#define BELOW(entity) (Vector2){entity->position.x, entity->position.y + 1.0f}
#define LEFT(entity) (Vector2){entity->position.x - 1.0f, entity->position.y}
#define RIGHT(entity) (Vector2){entity->position.x + 1.0f, entity->position.y}
#define FRONT(entity, faces_left) (Vector2){entity->position.x + (faces_left ? -1.0f : 1.0f), entity->position.y}
#define ABOVEFRONT(entity, faces_left) (Vector2){entity->position.x + (faces_left ? -1.0f : 1.0f), entity->position.y - 1.0f}
#define BELOWFRONT(entity, faces_left) (Vector2){entity->position.x + (faces_left ? -1.0f : 1.0f), entity->position.y + 1.0f}


void animate_attack(const Vector2 offset,
                    Entity *entity,
                    Sprite *sprite,
                    float scale,
                    float dt,
                    const LevelData *level,
                    bool debug_mode) {
    coroutine_t *co = &entity->co[CO_ANIMATE];

    float attack_frame_time = 0.05f;
    bool faces_left = entity->sprite->flip_x;

    Rectangle attack_hit_box = (Rectangle){0, 0, 1.0, 1.0};

    CO_START(co);
    // Draw frame above head
    case -1:
    draw_attack_at(offset, sprite, 0, ABOVE(entity), scale);
    check_and_register_collisions(entity, ABOVE(entity), level, attack_hit_box, scale, offset, debug_mode);
    CO_WAIT_CASE(co, attack_frame_time, dt, -1);

    // Above, and above to the right
    case -2:
    draw_attack_at(offset, sprite, 1, ABOVE(entity), scale);
    draw_attack_at(offset, sprite, 2, ABOVEFRONT(entity, faces_left), scale);
    check_and_register_collisions(entity, ABOVE(entity), level, attack_hit_box, scale, offset, debug_mode);
    check_and_register_collisions(entity, ABOVEFRONT(entity, faces_left), level, attack_hit_box, scale, offset, debug_mode);
    CO_WAIT_CASE(co, attack_frame_time, dt, -2);

    // Front
    case -3:
    draw_attack_at(offset, sprite, 3, FRONT(entity, faces_left), scale);
    check_and_register_collisions(entity, FRONT(entity, faces_left), level, attack_hit_box, scale, offset, debug_mode);
    CO_WAIT_CASE(co, attack_frame_time, dt, -3);

    // Front and below front
    case -4:
    draw_attack_at(offset, sprite, 4, FRONT(entity, faces_left), scale);
    draw_attack_at(offset, sprite, 0, BELOWFRONT(entity, faces_left), scale);
    check_and_register_collisions(entity, FRONT(entity, faces_left), level, attack_hit_box, scale, offset, debug_mode);
    check_and_register_collisions(entity, BELOWFRONT(entity, faces_left), level, attack_hit_box, scale, offset, debug_mode);
    CO_WAIT_CASE(co, attack_frame_time, dt, -4);

    // Below
    case -5:
    draw_attack_at(offset, sprite, 1, BELOW(entity), scale);
    check_and_register_collisions(entity, BELOW(entity), level, attack_hit_box, scale, offset, debug_mode);
    CO_WAIT_CASE(co, attack_frame_time, dt, -5);

    entity->is_attacking = false;

    CO_END(co);
}
