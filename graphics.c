#include "graphics.h"

void initialize_graphics(void) {
    graphics_data.tileset_image = LoadImage("assets/character and tileset/Dungeon_Tileset.png");
    graphics_data.tileset_texture = LoadTextureFromImage(graphics_data.tileset_image);
    graphics_data.tileset_size = (Rectangle){0, 0, 160, 160};

    graphics_data.characters_image = LoadImage("assets/character and tileset/Dungeon_Character_2.png");
    graphics_data.characters_texture = LoadTextureFromImage(graphics_data.characters_image);
}

void scale_rec(Rectangle *rec, float scale) {
    rec->x      *= scale;
    rec->y      *= scale;
    rec->width  *= scale;
    rec->height *= scale;
}

void draw(Element element, Vector2 position, float scale) {
    if ((element & EL_ROOM) != 0) {
        Rectangle room_src = {0, 0, TILE_SIZE * 6, TILE_SIZE * 5};
        Rectangle room_dst = {position.x, position.y,  TILE_SIZE * 6,  TILE_SIZE * 5};
        scale_rec(&room_dst, scale);
        DrawTexturePro(graphics_data.tileset_texture, room_src, room_dst, (Vector2){0, 0}, 0, WHITE);
    }
    if ((element & EL_WALL_1) != 0) {
        Rectangle wall_src = {TILE_SIZE, 0, TILE_SIZE, TILE_SIZE};
        Rectangle wall_dst = {position.x, position.y, TILE_SIZE, TILE_SIZE};
        scale_rec(&wall_dst, scale);
        DrawTexturePro(graphics_data.tileset_texture, wall_src, wall_dst, (Vector2){0, 0}, 0, WHITE);
    }
    if ((element & EL_UP_WALL_1) != 0) {
        Rectangle wall_src = {TILE_SIZE * 5, TILE_SIZE * 5, TILE_SIZE, TILE_SIZE};
        Rectangle wall_dst = {position.x, position.y, TILE_SIZE, TILE_SIZE};
        scale_rec(&wall_dst, scale);
        DrawTexturePro(graphics_data.tileset_texture, wall_src, wall_dst, (Vector2){0, 0}, 0, WHITE);
    }
    if ((element & EL_UP_WALL_2) != 0) {
        Rectangle wall_src = {TILE_SIZE * 4, TILE_SIZE * 5, TILE_SIZE, TILE_SIZE};
        Rectangle wall_dst = {position.x, position.y, TILE_SIZE, TILE_SIZE};
        scale_rec(&wall_dst, scale);
        DrawTexturePro(graphics_data.tileset_texture, wall_src, wall_dst, (Vector2){0, 0}, 0, WHITE);
    }
    if ((element & EL_FLOOR_1) != 0) {
        Rectangle floor_src = {TILE_SIZE * 7, TILE_SIZE, TILE_SIZE, TILE_SIZE};
        Rectangle floor_dst = {position.x, position.y, TILE_SIZE + 2, TILE_SIZE};
        scale_rec(&floor_dst, scale);
        DrawTexturePro(graphics_data.tileset_texture, floor_src, floor_dst, (Vector2){0, 0}, 0, WHITE);
    }
    if ((element & EL_FLOOR_2) != 0) {
        Rectangle floor_src = {TILE_SIZE * 7, TILE_SIZE, TILE_SIZE, TILE_SIZE};
        Rectangle floor_dst = {position.x - 2, position.y, TILE_SIZE + 2, TILE_SIZE};
        scale_rec(&floor_dst, scale);
        DrawTexturePro(graphics_data.tileset_texture, floor_src, floor_dst, (Vector2){0, 0}, 0, WHITE);
    }
    if ((element & EL_DOOR_LEFT_OPEN) != 0) {
        draw(EL_WALL_1, position, scale);
        draw(EL_FLOOR_1, Vector2Add(position, DOWN), scale);
        draw(EL_UP_WALL_1, Vector2Add(position, Vector2Add(DOWN, DOWN)), scale);
    }
    if ((element & EL_DOOR_RIGHT_OPEN) != 0) {
        draw(EL_WALL_1, position, scale);
        draw(EL_FLOOR_2, Vector2Add(position, DOWN), scale);
        draw(EL_UP_WALL_2, Vector2Add(position, Vector2Add(DOWN, DOWN)), scale);
    }
}


void draw_room_lrd(Vector2 position, float scale) {
    draw(EL_ROOM, position, scale);
    draw(EL_DOOR_LEFT_OPEN, Vector2Add(position, DOWN), scale);
    draw(EL_DOOR_RIGHT_OPEN, Vector2Add(position, (Vector2){5 * TILE_SIZE, TILE_SIZE}), scale);
}


void deinit_graphics(void) {
    UnloadTexture(graphics_data.tileset_texture);
    UnloadImage(graphics_data.tileset_image);
}
