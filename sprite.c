Sprite *load_sprite_char(const char *name) {
    char temp[256];
    char *parent_folder;
    if (strcmp(name, "priest1") == 0 || strcmp(name, "priest2") == 0 || strcmp(name, "priest3") == 0) {
        parent_folder = "priests_idle";
    } else {
        parent_folder = "monsters_idle";
    }

    Image *images = malloc(sizeof(Image) * 4);
    Texture2D *textures = malloc(sizeof(Texture2D) * 4);

    for (int i = 1; i <= 4; i++) {
        sprintf(temp, "assets/Character_animation/%s/%s/v2/%s_v2_%i.png", parent_folder, name, name, i);
        images[i - 1] = LoadImage(temp);
        textures[i - 1] = LoadTextureFromImage(images[i - 1]);
    }


    Sprite *sprite = malloc(sizeof(Sprite));
    sprite->images = images;
    sprite->textures = textures;
    sprite->frame_count = 4;
    sprite->frames_per_frame = 10;
    sprite->flip_x = false;


    return sprite;
}


Sprite *load_sprite(const char **name, int n_frames, int frames_per_frame) {

    Image *images = malloc(sizeof(Image) * n_frames);
    Texture2D *textures = malloc(sizeof(Texture2D) * n_frames);

    for (int i = 0; i < n_frames; i++) {
        images[i] = LoadImage(name[i]);
        textures[i] = LoadTextureFromImage(images[i]);
    }


    Sprite *sprite = malloc(sizeof(Sprite));
    sprite->images = images;
    sprite->textures = textures;
    sprite->frame_count = n_frames;
    sprite->frames_per_frame = frames_per_frame;
    sprite->flip_x = false;


    return sprite;
}

void animate(Sprite *sprite, int frame_count, Vector2 position, float scale) {
    int frame_number = (frame_count % (sprite->frame_count * sprite->frames_per_frame)) / sprite->frames_per_frame;
    if (frame_number > sprite->frame_count) {
        frame_number = 0;
    }
    int sign = sprite->flip_x ? -1 : 1;

    DrawTexturePro(sprite->textures[frame_number],
                   (Rectangle){0, 0, TILE_SIZE * sign, TILE_SIZE},
                   (Rectangle){position.x * scale * TILE_SIZE, position.y * scale * TILE_SIZE, TILE_SIZE * scale, TILE_SIZE * scale},
                   (Vector2){0, 0},
                   0,
                   WHITE);
}


void free_sprite(Sprite *sprite) {
    free(sprite->images);
    free(sprite->textures);
    free(sprite);
}
