/*
To use this file first #define DICTNAME, DICTKEYTYPE, and DICTVALTYPE.
DICTNAME will be the name of the dictionary type and will also
post-fix all function names, e.g. #define DICTNAME MyDict -> createMyDict/freeMyDict/etc.

DICTKEY/VALTYPE will be the type of the key/value for the dictionary.

If you redefine DICTNAME, DICTKEYTYPE, and DICTVALTYPE and re-include this file,
you can have multiple versions of a linked list, thus providing some form
of generics in C.
 */

#define FNAME(x, y) concat(x, y)
#define concat(x, y) x ## y


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>

typedef struct {
  DICTKEYTYPE key;
  DICTVALTYPE value;
} FNAME(KVP, DICTNAME);




#define HASHSEED 0
#define concat3(x, y, z) x ## y ## z
#define pconcat3(x, y, z) concat3(x, y, z)
#define LLISTTYPE FNAME(KVP, DICTNAME)
#define LLISTNAME pconcat3(__, DICTNAME, DICTLIST)
#define DLLISTNAME pconcat3(__, DICTNAME, DICTLIST)
//This many levels of nested macros is needed so the preprocessor
// correctly expands them
#include "linkedlist.c"

#ifndef HASH_SELECTOR
#define HASH_SELECTOR(x) (unsigned char *)&x
#endif

#ifndef HASHSIZE
#define HASHSIZE(x) sizeof(x)
#endif

#define getkey(llnode) llnode->value.key
#define getval(llnode) llnode->value.value


#include "utils.c"

typedef struct {
  int nbuckets;
  int bits;
  LLISTNAME **buckets;
} DICTNAME;


DICTNAME * FNAME(create, DICTNAME)(int nbuckets) {
  DICTNAME *dict = malloc(sizeof(DICTNAME));

  int bits = getHighestBit(nbuckets);

  if ((1 << bits) != nbuckets) {
    bits++;
  }

  dict->nbuckets = 1 << bits;
  dict->bits = bits;
  dict->buckets = malloc(dict->nbuckets * sizeof(LLISTNAME *));
  for (int i = 0; i < dict->nbuckets; i++) {
    dict->buckets[i] = NULL;
  }

  return dict;
}

void FNAME(free, DICTNAME)(DICTNAME *dict) {
  assert(dict != NULL);
  for (int i = 0; i < dict->nbuckets; i++) {
    if (dict->buckets[i] != NULL) {
      FNAME(free, LLISTNAME)(dict->buckets[i]);
    }
  }
  free(dict->buckets);
  free(dict);
}


#include "hashing.c"


bool FNAME(bucketContainsKey, DICTNAME)(LLISTNAME * bucket, DICTKEYTYPE key) {
  LLISTNAME * current = bucket;
  while (current != NULL) {
    if (FNAME(DICTKEYTYPE, _EQ)(getkey(current), key)) {
      return true;
    }
    current = current->next;
  }
  return false;
}

bool FNAME(containsKey, DICTNAME)(DICTNAME * dict, DICTKEYTYPE key) {
  int bucketNumber = hashFunction(HASH_SELECTOR(key), HASHSIZE(key), HASHSEED, dict->bits);
  if (dict->buckets[bucketNumber] == NULL) {
    return false;
  }
  
  return FNAME(bucketContainsKey, DICTNAME)(dict->buckets[bucketNumber], key);
}


void FNAME(insertKVP, DICTNAME)(DICTNAME * dict, DICTKEYTYPE key, DICTVALTYPE val) {
  int bucketNumber = hashFunction(HASH_SELECTOR(key), HASHSIZE(key), HASHSEED, dict->bits);
  
  if (FNAME(containsKey, DICTNAME)(dict, key)) {
    for (LLISTNAME * node = dict->buckets[bucketNumber]; node != NULL; node = node->next) {
      if ( FNAME(DICTKEYTYPE, _EQ)(getkey(node), key)) {
        node->value.value = val;
        break;
      }
    }
  } else {
    FNAME(KVP, DICTNAME) kvp = {key, val};
    if (dict->buckets[bucketNumber] == NULL) {
      dict->buckets[bucketNumber] = FNAME(create, LLISTNAME)(kvp);
    } else {
      dict->buckets[bucketNumber] = FNAME(cons, LLISTNAME)(dict->buckets[bucketNumber], kvp);
    }
  }

  assert(dict->buckets[bucketNumber] != NULL);
}


LLISTNAME * FNAME(bucketDeleteKey, DICTNAME)(LLISTNAME * head, DICTKEYTYPE key) {
  if (FNAME(DICTKEYTYPE, _EQ)(getkey(head), key)) {
    LLISTNAME * newHead = head->next;
    FNAME(freeNode, LLISTNAME)(head);
    return newHead;
  } else {
    LLISTNAME * prev = head;
    LLISTNAME * curr = head->next;
    while (curr != NULL) {
      if (FNAME(DICTKEYTYPE, _EQ)(getkey(curr), key)) {
        prev->next = curr->next;
        FNAME(freeNode, LLISTNAME(curr));
        break;
      }
      prev = curr;
      curr = curr->next;
    }
    return head;              
  }
}


void FNAME(removeKey, DICTNAME)(DICTNAME * dict, DICTKEYTYPE key) {
  int bucketNumber = hashFunction(HASH_SELECTOR(key), HASHSIZE(key), HASHSEED, dict->bits);

  LLISTNAME * newHead = FNAME(bucketDeleteKey, DICTNAME)(dict->buckets[bucketNumber], key);
  dict->buckets[bucketNumber] = newHead;
  return;
}

DICTVALTYPE FNAME(getVal, DICTNAME)(DICTNAME * dict, DICTKEYTYPE key) {
  assert( FNAME(containsKey, DICTNAME)(dict, key) );

  int bucketNumber = hashFunction(HASH_SELECTOR(key), HASHSIZE(key), HASHSEED, dict->bits);

  for (LLISTNAME * node = dict->buckets[bucketNumber]; node != NULL; node = node->next) {
    if ( FNAME(DICTKEYTYPE, _EQ)(getkey(node), key)) {
      return getval(node);
    }
  }

  assert(0 && "Library error");
}

DICTVALTYPE FNAME(getValDefault, DICTNAME)(DICTNAME * dict, DICTKEYTYPE key, DICTVALTYPE defaultVal) {
  if (!FNAME(containsKey, DICTNAME)(dict, key)) {
    return defaultVal;
  }

  int bucketNumber = hashFunction(HASH_SELECTOR(key), HASHSIZE(key), HASHSEED, dict->bits);

  for (LLISTNAME * node = dict->buckets[bucketNumber]; node != NULL; node = node->next) {
    if ( FNAME(DICTKEYTYPE, _EQ)(getkey(node), key)) {
      return getval(node);
    }
  }

  assert(0 && "Library error");
}


// Loop over dict where each entry/node is a KVP
#define LOOPDICT(dict, nodename, body) \
for (int __IDX = 0; __IDX < dict->nbuckets; __IDX++) { \
    if (dict->buckets[__IDX] == NULL) { \
      continue; \
    } else { \
      for (DLLISTNAME * nodename = dict->buckets[__IDX]; nodename != NULL; nodename = nodename ->next) { \
        body \
      } \
    } \
 } \

