#ifndef ITEM_H
#define ITEM_H
#include "collision_box.h"


typedef enum ItemKind {
    ITEM_DOOR,
    ITEM_COIN,
    ITEM_KIND_COUNT,
} ItemKind;

// typedef union ItemVariant {
    // struct {
        // int tile_id_1;
        // int tile_id_2;
        // int active_version;
    // } door;
    // struct {
        // int tile_id_1;
        // int tile_id_2;
        // int active_version;
    // } Door;
// } ItemVariant;

typedef struct Item {
    int tile_id_1;
    int tile_id_2;
    int active_version;
    int x;
    int y;
    CollisionBox *collision_box;

    ItemKind item_kind;
} Item;


void item_update(Item * item);
Item setup_item(int main_id, CollisionBox *collision_box, int x, int y);

#endif
