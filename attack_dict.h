#ifndef ATTACK_DICT_H
#define ATTACK_DICT_H
#include "entity.h"

typedef uint64_t EntityID;
typedef struct Attack {
    EntityID id;
    int damage;
    float time;
    bool is_active;
} Attack;
#define DICTKEYTYPE EntityID
#define EntityID_EQ(a, b) ((a) == (b))
#define DICTVALTYPE Attack 
#define DICTNAME EntityIDAtkDict
#include "dictionary.c"
#endif
