#ifndef GAME_STATE_H
#define GAME_STATE_H
#include "editor_struct.h"
#include "level.h"
#include "player.h"

typedef struct GameState {
    int frame_count;
    Player player;
    LevelData level;
    float scale;
    Rectangle window;

    Vector2 camera_center;

    bool editor_enabled;
    Editor editor;

    bool is_paused;
} GameState;

// Global state
GameState game_state;

#endif
