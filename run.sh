#!/bin/bash
set -e

arg=$1

if [[ "$OSTYPE" == "darwin23" ]]; then
    FLAGS=$(pkg-config --libs --cflags raylib)
    CFLAGS="-Wall -Wpedantic -Wextra -std=c11"
    COROFLAGS="-Wno-implicit-fallthrough -Wno-unused-label"
    # LDFLAGS="-lraylib -lGL -lm -lpthread -ldl -lrt"
    gcc -o main main.c $FLAGS $CFLAGS $COROFLAGS $LDFLAGS -glldb
    if [[ "$arg" == "build" ]]; then
        exit 0
    fi
    ./main
else 
    INCLUDE="-I/opt/raylib/src"
    LIBS="-L/opt/raylib/src"
    CFLAGS="-Wall -Wpedantic -Wextra -std=c11 -D_DEFAULT_SOURCE"
    # CFLAGS="-Wall -Wpedantic -Wextra"
    COROFLAGS="-Wno-implicit-fallthrough -Wno-unused-label"
    LDFLAGS="-lraylib -lGL -lm -lpthread -ldl -lrt"
    if [[ "$arg" == "build" ]]; then
        exit 0
    fi
    gcc -o main main.c $INCLUDE $LIBS $CFLAGS $COROFLAGS $LDFLAGS -ggdb
    ./main
fi
