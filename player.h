#ifndef PLAYER_H
#define PLAYER_H
#include "entity.h"
#include "sprite.h"

typedef struct Player {
    Entity *entity;
    Sprite *sprite;

    float summon_timer;
    int summons_capacity;
} Player;


#endif
