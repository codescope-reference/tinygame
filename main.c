#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include "raylib.h"
#include "raymath.h"
#include "coroutine.h"
#include <dirent.h>


#include "controls.h"
#include "editor.h"
#include "game_state.h"
#include "graphics.h"
#include "item.h"
#include "level.h"
#include "tiles.h"

#include "controls.c"
#include "demo_animate.c"
#include "demo_room.c"
#include "editor.c"
#include "entity.c"
#include "graphics.c"
#include "item.c"
#include "level.c"
#include "sprite.c"





// TODO: We are stack allocating a lot of vectors in each frame. Maybe we should allocate them once and reuse them?
void draw_entity(const Vector2 position_offset, const Entity entity, float scale, int frame_count) {
    // TODO Inline?
    if (entity.is_dead) {
        return;
    }
    animate(entity.sprite, frame_count, Vector2Add(entity.position, position_offset), scale);
}

void draw_tile(int tile_id, float x, float y, float scale) {
    if (tile_id < 0) {
        return;
    }
    Rectangle room_src = {TILE_SIZE * (tile_id % 10), TILE_SIZE * (tile_id / 10), TILE_SIZE, TILE_SIZE};
    Rectangle room_dest = {x * TILE_SIZE * scale, y * TILE_SIZE * scale, TILE_SIZE * scale, TILE_SIZE * scale};
    DrawTexturePro(graphics_data.tileset_texture, room_src, room_dest, (Vector2){0, 0}, 0, WHITE);
}


bool is_adjacent(float x1, float y1, float x2, float y2) {
    return (fabs(x1 - x2) + fabs(y1 - y2)) <= 1.5;
}

bool is_adjacent2(float x1, float y1, float x2, float y2) {
    bool b1 = fabs(x1 - x2) <= 1.5;
    bool b2 = fabs(y1 - y2) <= 1.5;
    return b1 && b2;
}

void draw_level(Player *player, const Vector2 position_offset, const LevelData *level, const float scale, const int frame_count, bool is_paused) {
    for (int i = 0; i < level->n_tiles; i++) {
        int tile_id = level->tile_placements[i * 3 + 0];
        float x = (float)level->tile_placements[i * 3 + 1] + position_offset.x;
        float y = (float)level->tile_placements[i * 3 + 2] + position_offset.y;
        draw_tile(tile_id, x, y, scale);
    }


    // Draw items
    for (int i = 0; i < level->n_items; i++) {
        int active_version = level->items[i].active_version;
        float x = (float)level->items[i].x + position_offset.x;
        float y = (float)level->items[i].y + position_offset.y;
        draw_tile(active_version, x, y, scale);

        // Interaction code
        // If the player is close to the item, make it interactive
        // Close meaning if the player is on an adjacent tile
        if (is_adjacent2(player->entity->position.x, player->entity->position.y, level->items[i].x, level->items[i].y)) {
            item_update(&level->items[i]);
        }
    }


    // Draw enemies
    for (int i = 0; i < level->entity_count; i++) {
        Entity entity = level->entities[i];
        draw_entity(position_offset, entity, scale, is_paused ? 0 : frame_count);
    }

    // Draw summons
    for (int i = 0; i < level->summons_count; i++) {
        Entity entity = level->summons[i];
        draw_entity(position_offset, entity, scale, is_paused ? 0 : frame_count);
    }
}

void game_loop(GameState *state) {
    Entity *player = state->player.entity;
    Sprite *player_attack_sprite = state->player.sprite;
    LevelData *level = &state->level;
    float scale = state->scale;
    Rectangle window = state->window;
    int frame_count = state->frame_count;

    // Update code
    float dt = GetFrameTime();
    if (!player->is_dead && !state->is_paused) {
        update_entities(level, player, dt);
        update_summons(level, player, dt);
        update_player(&state->player, state, dt);

        // Follow player
        state->camera_center = (Vector2){-player->position.x, -player->position.y};
    }

    // Draw code
    ClearBackground((Color){51, 51, 51, 255});


    float w_tiles = (float)window.width / (TILE_SIZE * scale);
    float h_tiles = (float)window.height / (TILE_SIZE * scale);
    Vector2 center_offset = (Vector2){w_tiles / 2 - 0.5, h_tiles / 2 - 0.5};
    if (player->is_dead) {
        DrawText("You died!", window.width/2 - 20, window.height / 2, 20, RED);
    } else {
        draw_level(&state->player,
                   Vector2Add(center_offset,
                   state->camera_center),
                   level,
                   scale,
                   frame_count,
                   state->is_paused);
        // draw_demo_room(&level, scale, frame_count);
        
        // Draw player
        // Get width and height in number of tiles
        animate(player->sprite, state->is_paused ? 0 : frame_count, 
                Vector2Add(center_offset, Vector2Add(player->position, state->camera_center)), scale);

        if (player->is_attacking) {
            animate_attack(
                    Vector2Add(center_offset, state->camera_center),
                    player, player_attack_sprite, scale, dt, level, level->debug_mode);
        }



        // TODO: Refactor into dev mode
        if (level->debug_mode) {
            // Also draw collision boxes
            for (int i = 0; i < level->n_collision_boxes; i++) {
                if (!level->collision_boxes[i].is_active) {
                    continue;
                }

                Vector2 wall = level->collision_boxes[i].position;
                wall.x += center_offset.x + state->camera_center.x;
                wall.y += center_offset.y + state->camera_center.y;
                DrawRectangleLines(wall.x * TILE_SIZE * scale, wall.y * TILE_SIZE * scale, TILE_SIZE * scale, TILE_SIZE * scale, RED);
            }

            // Draw player collision box
            Rectangle player_collision = {
                (player->position.x + player->collision_box.x) * TILE_SIZE,
                (player->position.y + player->collision_box.y) * TILE_SIZE,
                (player->collision_box.width) * TILE_SIZE,
                (player->collision_box.height) * TILE_SIZE
            };
            player_collision.x += (state->camera_center.x + center_offset.x) * TILE_SIZE;
            player_collision.y += (state->camera_center.y + center_offset.y) * TILE_SIZE;
            DrawRectangleLines(player_collision.x * scale, player_collision.y * scale, player_collision.width * scale, player_collision.height * scale, RED);

            // Draw collision boxes of entities
            for (int i = 0; i < level->entity_count; i++) {
                Entity entity = level->entities[i];
                if (entity.is_dead) {
                    continue;
                }

                Rectangle entity_collision = {
                    (entity.position.x + entity.collision_box.x) * TILE_SIZE,
                    (entity.position.y + entity.collision_box.y) * TILE_SIZE,
                    (entity.collision_box.width) * TILE_SIZE,
                    (entity.collision_box.height) * TILE_SIZE
                };
                entity_collision.x += (state->camera_center.x + center_offset.x) * TILE_SIZE;
                entity_collision.y += (state->camera_center.y + center_offset.y) * TILE_SIZE;
                DrawRectangleLines(entity_collision.x * scale, entity_collision.y * scale, entity_collision.width * scale, entity_collision.height * scale, RED);
            }
        }
    }



    DrawFPS(10, 10);

    if (IsKeyReleased(KEY_Z)) {
        state->level.debug_mode = !state->level.debug_mode;
    }

    if (IsKeyReleased(KEY_SPACE)) {
        player->is_attacking = true;
    }
    if (IsKeyReleased(KEY_E)) {
        state->editor_enabled = true;
    }
    if (IsKeyReleased(KEY_C)) {
        // if (level->summons_count < level->summons_capacity) {

            // add_summon(state, player->position);
        // } 
        printf("Level summons count: %d\n", state->level.summons_count);

        int num_alive = 0;
        for (int i = 0; i < state->level.summons_capacity; i++) {
            if (state->level.summons[i].health > 0) {
                num_alive++;
            }
        }

        printf("Number of alive summons: %d\n", num_alive);

    }



    if (IsKeyReleased(KEY_R)) {
        // Restart level
        // Right now that is just putting the player at the start position
        // and resetting the player health
        // and reviving the entities
        player->position = level->starting_point;
        player->health = 10;
        player->is_dead = false;
        for (int i = 0; i < level->entity_count; i++) {
            level->entities[i].is_dead = false;
            level->entities[i].health = 10;
        }
    }

    if (IsKeyReleased(KEY_B)) {
        // B for Break
        state->is_paused = !state->is_paused;
    }

    if (IsKeyReleased(KEY_P)) {
        TakeScreenshot("screenshot.png");
    }

    handle_input(player);
}





int main(void) {
    Rectangle window = {0, 0, 1200, 800};
    InitWindow(window.width, window.height, "TinyGame");
    SetTargetFPS(60);


    initialize_graphics();

    Sprite *player_sprite = load_sprite_char("priest3");
    Rectangle collision_box = (Rectangle){1.0 / 16.0,1.0 / 16.0, 1.0 - 2.0 / 16.0, 1.0 - 2.0 / 16.0};
    Entity player = create_entity(player_sprite, (Vector2){5, 5}, 10, collision_box);


    const char *player_attack_names[] = {
        "assets/items and trap_animation/keys/keys_1_1.png",
        "assets/items and trap_animation/keys/keys_1_2.png",
        "assets/items and trap_animation/keys/keys_1_3.png",
        "assets/items and trap_animation/keys/keys_1_4.png",
        "assets/items and trap_animation/keys/keys_1_1.png",
    };
    Sprite *player_attack_sprite = load_sprite(player_attack_names, 5, -1);
    printf("Loaded attacked sprite successfully\n");


    LevelData level;
    load_demo_room(&level);

    game_state.player = (Player){.entity = &player,
                                 .sprite = player_attack_sprite,
                                 .summon_timer = 0.5f,
                                 .summons_capacity = 3};
    game_state.level = level;
    game_state.frame_count = 0;
    game_state.scale = 4.0f;
    game_state.window = window;
    game_state.editor_enabled = false;
    game_state.editor = (Editor){
        .tile_placements = {0},
        .n_tiles = 0,
        .collision_boxes = {0},
        .n_collision_boxes = 0,
        .starting_point = (Vector2){0, 0},
        .starting_point_placed = false,
    };
    game_state.is_paused = false;

    for (int i = 0; i < TILE_EDITOR_CAPACITY; i++) {
        game_state.editor.tile_placements[i][0] = -1;
        game_state.editor.placed_characters[i][0] = -1;
    }


    while(!WindowShouldClose()) {
        BeginDrawing();

        game_state.frame_count++;

        if (game_state.editor_enabled) {
            editor(&game_state);
        } else {
            game_loop(&game_state);
        }
        

        game_state.frame_count = game_state.frame_count % 1000;


        // if (!game_state.editor.save_level_open) { //TODO Make "controls enabled" calc
        // if (IsKeyReleased(KEY_T)) {
        //     // Dump info about level, skip arrays
        //     printf("Level info:\n");
        //     printf("Level entity count: %d\n", game_state.level.entity_count);
        //     printf("Level tile count: %d\n", game_state.level.n_tiles);
        //     printf("Level collision box count: %d\n", game_state.level.n_collision_boxes);
        //     int active_collision_boxes = 0;
        //     for (int i = 0; i < game_state.level.n_collision_boxes; i++) {
        //         if (game_state.level.collision_boxes[i].is_active) {
        //             active_collision_boxes++;
        //         }
        //         printf("Collision box %d: (%f, %f)\n", i, game_state.level.collision_boxes[i].position.x, game_state.level.collision_boxes[i].position.y);
        //     }
        //     printf("Level active collision box count: %d\n", active_collision_boxes);
        //     printf("Player position: (%f, %f)\n", game_state.player.entity->position.x, game_state.player.entity->position.y);
        //     printf("Level starting point: (%f, %f)\n", game_state.level.starting_point.x, game_state.level.starting_point.y);


        //     for (int i = 0; i < game_state.level.n_items; i++) {
        //         printf("Item %d: Is adjacent? %d\n", i, is_adjacent(game_state.player.entity->position.x, game_state.player.entity->position.y, game_state.level.items[i].x, game_state.level.items[i].y));
        //     }

        // }
        // }

        EndDrawing();

        // Controls valid in any mode 
        if (IsKeyReleased(KEY_KP_ADD)) {
            game_state.scale += 0.1f;
        }
        if (IsKeyReleased(KEY_KP_SUBTRACT)) {
            game_state.scale -= 0.1f;
        }
    }


    free_sprite(player_attack_sprite);
    // TODO Handle freeing room correctly if we freed it in the editor
    // unload_demo_room(&level);
    deinit_graphics();
    CloseWindow();
}
