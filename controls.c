#include "controls.h"


void add_summon(GameState *state, Vector2 position) {
    Sprite *summon_sprite = malloc(sizeof(Sprite));
    summon_sprite->images = malloc(sizeof(Image) * 4);


    // TODO: Reuse loaded images when creating new summons of the same type
    // TODO: Make a global cache of images that entities use to look up images and textures
    summon_sprite->images[0] = LoadImage("assets/Character_animation/monsters_idle/skull/v1/skull_v1_1.png");
    summon_sprite->images[1] = LoadImage("assets/Character_animation/monsters_idle/skull/v1/skull_v1_2.png");
    summon_sprite->images[2] = LoadImage("assets/Character_animation/monsters_idle/skull/v1/skull_v1_3.png");
    summon_sprite->images[3] = LoadImage("assets/Character_animation/monsters_idle/skull/v1/skull_v1_4.png");
    summon_sprite->textures = malloc(sizeof(Texture2D) * 4);
    summon_sprite->textures[0] = LoadTextureFromImage(summon_sprite->images[0]);
    summon_sprite->textures[1] = LoadTextureFromImage(summon_sprite->images[1]);
    summon_sprite->textures[2] = LoadTextureFromImage(summon_sprite->images[2]);
    Texture2D texture = LoadTextureFromImage(summon_sprite->images[3]);
    summon_sprite->textures[3] = texture;
    summon_sprite->frame_count = 4;
    summon_sprite->frames_per_frame = 10;
    summon_sprite->flip_x = false;

    Entity summon = {
        .sprite = summon_sprite,
        .position = position,
        .velocity = (Vector2){0, 0},
        .health = 3,
        .is_dead = false,
        .is_attacking = false,
        .is_invincible = false,
        .attacks = createEntityIDAtkDict(256),
        .collision_box = (Rectangle){1.0f / 16.0f,1.0f / 16.0f, 1.0f - 1.0f / 16.0f, 1.0f - 1.0f / 16.0f},
        .id = get_next_id(),
    };


    state->level.summons[state->level.summons_count++] = summon;
}




void handle_input(Entity *entity) {
    if (IsKeyDown(KEY_W)) {
        entity->velocity.y = -5;
    } else if (IsKeyDown(KEY_S)) {
        entity->velocity.y = 5;
    } else {
        entity->velocity.y = 0;
    }

    if (IsKeyDown(KEY_A)) {
        entity->velocity.x = -5;
    } else if (IsKeyDown(KEY_D)) {
        entity->velocity.x = 5;
    } else {
        entity->velocity.x = 0;
    }
}


// TODO Rename to move_with_collisions
void update(Entity *entity, LevelData *level, float dt) {
    // Try to move entity, check for collisions
    if (entity->velocity.x < 0) {
        entity->sprite->flip_x = true;
    } else if (entity->velocity.x > 0) {
        entity->sprite->flip_x = false;
    }

    float new_x = entity->position.x + entity->velocity.x * dt + entity->collision_box.x;
    float new_y = entity->position.y + entity->velocity.y * dt + entity->collision_box.y;
    float lower_x = new_x + entity->collision_box.width;
    float lower_y = new_y + entity->collision_box.height;
    for (int i = 0; i < level->n_collision_boxes; i++) {
        if (!level->collision_boxes[i].is_active) {
            continue;
        }

        Vector2 wall = level->collision_boxes[i].position;
        bool upper_left_collides = new_x >= wall.x && new_x <= wall.x + 1.0f && new_y >= wall.y && new_y <= wall.y + 1.0f;
        bool lower_left_collides = new_x >= wall.x && new_x <= wall.x + 1.0f && lower_y >= wall.y && lower_y <= wall.y + 1.0f;
        bool upper_right_collides = lower_x >= wall.x && lower_x <= wall.x + 1.0f && new_y >= wall.y && new_y <= wall.y + 1.0f;
        bool lower_right_collides = lower_x >= wall.x && lower_x <= wall.x + 1.0f && lower_y >= wall.y && lower_y <= wall.y + 1.0f;

        bool collide_right = upper_right_collides || lower_right_collides;
        bool collide_left = upper_left_collides || lower_left_collides;
        bool collide_up = upper_left_collides || upper_right_collides;
        bool collide_down = lower_left_collides || lower_right_collides;

        if (collide_right && entity->velocity.x > 0) {
            entity->velocity.x = 0;
        } else if (collide_left && entity->velocity.x < 0) {
            entity->velocity.x = 0;
        }

        if (collide_up && entity->velocity.y < 0) {
            entity->velocity.y = 0;
        } else if (collide_down && entity->velocity.y > 0) {
            entity->velocity.y = 0;
        }
    }

    entity->position.x += entity->velocity.x * dt;
    entity->position.y += entity->velocity.y * dt;
}


void update_entities(LevelData *level, Entity *player, const float dt) {
    for (int i = 0; i < level->entity_count; i++) {
        Entity *entity = &level->entities[i];
        if (entity->is_dead) {
            continue;
        }

        if (entity->health <= 0) {
            entity->is_dead = true;
            level->num_dead++; // Later this will be used to initiate clean up
            continue;
        }

        // AI: Move towards player
        float distance_threshold = 0.05f;
        if (entity->position.x < player->position.x - distance_threshold) {
            entity->velocity.x = 1;
        } else if (entity->position.x > player->position.x + distance_threshold) {
            entity->velocity.x = -1;
        } else {
            entity->velocity.x = 0;
        }
        if (entity->position.y < player->position.y - distance_threshold) {
            entity->velocity.y = 1;
        } else if (entity->position.y > player->position.y + distance_threshold) {
            entity->velocity.y = -1;
        } else {
            entity->velocity.y = 0;
        }
        // Normalize the velocity
        float length = sqrt(entity->velocity.x * entity->velocity.x + entity->velocity.y * entity->velocity.y);
        if (length > 0) {
            entity->velocity.x /= length;
            entity->velocity.y /= length;
        }
        
        
        update(entity, level, dt);


        float time = GetTime();
        float delay_time = 0.2;
        EntityID to_remove[256];
        int to_remove_count = 0;
        LOOPDICT(entity->attacks, kvp_node, {
                Attack attack = kvp_node->value.value;

                if (attack.is_active) {
                 // Apply damage
                 Attack inactive_attack = attack;
                 inactive_attack.is_active = false;
                 insertKVPEntityIDAtkDict(entity->attacks, kvp_node->value.key, inactive_attack);
                 entity->health -= attack.damage;
                }

                // Must be after above
                float dt = time - attack.time;
                if (!attack.is_active && dt >= delay_time) {
                    // Can be removed
                    if (to_remove_count < 256) {
                        to_remove[to_remove_count++] = kvp_node->value.key;
                    }
                }

        });

        for (int i = 0; i < to_remove_count; i++) {
            if (containsKeyEntityIDAtkDict(entity->attacks, to_remove[i])) {
                removeKeyEntityIDAtkDict(entity->attacks, to_remove[i]);
            }
        }




        Rectangle player_collision = {
            (player->position.x + player->collision_box.x),
            (player->position.y + player->collision_box.y),
            (player->collision_box.width),
            (player->collision_box.height) 
        };
        Rectangle enemy_collision = {
            (entity->position.x + entity->collision_box.x),
            (entity->position.y + entity->collision_box.y),
            (entity->collision_box.width),
            (entity->collision_box.height) 
        };
        // TODO Maybe use attack registration system instead of entity-global timer
        if (!player->is_invincible && CheckCollisionRecs(player_collision, enemy_collision)) {
            player->health -= 1;
            player->is_invincible = true;
            printf("Player health: %d\n", player->health);
            // TODO: Push back player
            // TODO: Sound effect
        }
    }
}


void steer_towards(Entity *entity, Vector2 target, distance_threshold) {
    if (entity->position.x < target.x - distance_threshold) {
        entity->velocity.x = 1;
    } else if (entity->position.x > target.x + distance_threshold) {
        entity->velocity.x = -1;
    } else {
        entity->velocity.x = 0;
    }
    if (entity->position.y < target.y - distance_threshold) {
        entity->velocity.y = 1;
    } else if (entity->position.y > target.y + distance_threshold) {
        entity->velocity.y = -1;
    } else {
        entity->velocity.y = 0;
    }
}

void update_summons(LevelData * level, Entity * player, const float dt) {
    // TODO: If there are no more enemies, then make summons follow/circle player
    for (int i = 0; i < level->summons_count; i++) {
        Entity *summon = &level->summons[i];
        if (summon->is_dead) {
            level->summons_count--;
            level->num_dead_summons++; // Later this will be used to initiate clean up
            continue;
        }

        if (summon->health <= 0) {
            summon->is_dead = true;
            continue;
        }

        Rectangle summon_collision = {
            (summon->position.x + summon->collision_box.x),
            (summon->position.y + summon->collision_box.y),
            (summon->collision_box.width),
            (summon->collision_box.height) 
        };

        // AI: Move towards closest enemy
        float distance_threshold = 0.05f;
        float closest_distance = 1000000;
        Vector2 closest_position = {0, 0};
        for (int e_idx = 0; e_idx < level->entity_count; e_idx++) { // TODO: Use a quadtree
            Entity *enemy = &level->entities[e_idx];
            if (enemy->is_dead) {
                continue;
            }

            // Find closest enemy for summon to move towards
            Vector2 e_pos = enemy->position;
            float dx = e_pos.x - summon->position.x;
            float dy = e_pos.y - summon->position.y;
            float distance = dx * dx + dy * dy;

            if (distance < closest_distance) {
                closest_distance = distance;
                closest_position = e_pos;
            }


            // Check for collision while we're looping through enemies anyway.
            // Register any attacks

            Rectangle enemy_collision = {
                (enemy->position.x + enemy->collision_box.x),
                (enemy->position.y + enemy->collision_box.y),
                (enemy->collision_box.width),
                (enemy->collision_box.height) 
            };

            if (CheckCollisionRecs(summon_collision, enemy_collision)) {
                // TODO: 
                // Register an attack on both summon and enemy
                register_attack_on(summon, enemy, 1);
                register_attack_on(enemy, summon, 2);
            }
        }

        float distance = sqrt(closest_distance);
        if (distance > 5) {
            // Go towards player
            float distance_threshold = 1.0f;
            float radius = 3.0f;
            float time = GetTime();
            float angular_speed = 1.57 * (i+ 1);
            Vector2 direction = {.x = radius * cosf(time * angular_speed), .y = radius * sinf(time * angular_speed)};
            Vector2 target = Vector2Add(player->position, direction);
            steer_towards(summon, target, distance_threshold);
        } else {
            // Go towards that enemy
            float distance_threshold = 0.05f;
            steer_towards(summon, closest_position, distance_threshold);
        }
        // Normalize the velocity
        float length = sqrt(summon->velocity.x * summon->velocity.x + summon->velocity.y * summon->velocity.y);
        float summon_speed = 5.0f;
        if (length > 0) {
            summon->velocity.x *= summon_speed / length;
            summon->velocity.y *= summon_speed / length;
        }
        
        
        update(summon, level, dt);


        float time = GetTime();
        float delay_time = 0.2;
        EntityID to_remove[256];
        int to_remove_count = 0;
        LOOPDICT(summon->attacks, kvp_node, {
                Attack attack = kvp_node->value.value;

                if (attack.is_active) {
                 // Apply damage
                 Attack inactive_attack = attack;
                 inactive_attack.is_active = false;
                 insertKVPEntityIDAtkDict(summon->attacks, kvp_node->value.key, inactive_attack);
                 summon->health -= attack.damage;
                }

                // Must be after above
                float dt = time - attack.time;
                if (!attack.is_active && dt >= delay_time) {
                    // Can be removed
                    if (to_remove_count < 256) {
                        to_remove[to_remove_count++] = kvp_node->value.key;
                    }
                }

        });

        for (int i = 0; i < to_remove_count; i++) {
            if (containsKeyEntityIDAtkDict(summon->attacks, to_remove[i])) {
                removeKeyEntityIDAtkDict(summon->attacks, to_remove[i]);
            }
        }
    }
}


void update_player(Player *player, GameState *state, float dt) {
    // LevelData level = state->level;
    Entity *entity = player->entity;
    update(entity, &state->level, dt);
    if (entity->is_invincible) {
        float wait_time = 0.2;
        float dt = GetFrameTime();
        coroutine_t *co = &entity->co[CO_TIMER];

        CO_START(co);
        CO_WAIT(co, wait_time, dt);
        entity->is_invincible = false;
        CO_END(co);
    }
    if (entity->health <= 0) {
        entity->is_dead = true;
    }

    if (state->level.summons_count < player->summons_capacity && player->summon_timer > 0.0) {
        player->summon_timer -= dt;
        // printf("summon_timer: %f\n", player->summon_timer);
    }

    if (state->level.summons_count < state->level.summons_capacity &&
        state->level.summons_count < player->summons_capacity &&
        player->summon_timer <= 0.0) {
        player->summon_timer = 0.5f;
        add_summon(state, player->entity->position);
    }
}
