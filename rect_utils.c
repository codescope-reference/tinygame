#include "raylib.h"



Rectangle rect_right(Rectangle rect, int spacing) {
    return (Rectangle) {
        .x = rect.x + rect.width + spacing,
        .y = rect.y,
        .width = rect.width,
        .height = rect.height
    };
}


Rectangle rect_below(Rectangle rect, int spacing) {
    return (Rectangle) {
        .x = rect.x,
        .y = rect.y + rect.height + spacing,
        .width = rect.width,
        .height = rect.height
    };
}


Rectangle rect_bottom_left(Rectangle rect, int width, int height, int spacing_x, int spacing_y) {
    return (Rectangle) {
        .x = rect.x + spacing_x,
        .y = rect.y + rect.height - spacing_y - height,
        .width = width,
        .height = height
    };
}


bool rect_clicked(Rectangle rect) {
    return CheckCollisionPointRec(GetMousePosition(), rect) && IsMouseButtonReleased(MOUSE_LEFT_BUTTON);
}
