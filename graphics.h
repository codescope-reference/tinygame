#ifndef GRAPHICS_H
#define GRAPHICS_H
const int TILE_SIZE = 16;

typedef struct GraphicsData {
    Image tileset_image;
    Texture2D tileset_texture;
    Rectangle tileset_size;


    Image characters_image;
    Texture2D characters_texture;
} GraphicsData;



void initialize_graphics(void);
typedef enum Element {
    EL_ROOM            = 1 << 0,
    EL_DOOR_LEFT_OPEN  = 1 << 1,
    EL_DOOR_RIGHT_OPEN = 1 << 2,
    EL_WALL_1          = 1 << 3,
    EL_FLOOR_1         = 1 << 4,
    EL_FLOOR_2         = 1 << 5,
    EL_UP_WALL_1       = 1 << 6,
    EL_UP_WALL_2       = 1 << 7,
} Element;

const Vector2 DOWN = {0, TILE_SIZE};

void scale_rec(Rectangle *rec, float scale);
void draw(Element element, Vector2 position, float scale);
void draw_room_lrd(Vector2 position, float scale);
void deinit_graphics(void);



// TODO Should this be global? Put into GameState?
GraphicsData graphics_data;
#endif
