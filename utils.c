#pragma once


int getHighestBit(int n) {
  int bits = 0;
  for (size_t i = 0; i < 8 * sizeof(int); i++) {
    int bitval = (n >> i) & 1;
    if (bitval != 0) {
      bits = i;
    }
  }

  return bits;
}
