#ifndef CONTROLS_H
#define CONTROLS_H
#include "entity.h"
#include "level.h"
#include "player.h"
#include "game_state.h"

void handle_input(Entity *entity);
// TODO Rename to move_with_collisions
void update(Entity *entity, LevelData *level, float dt);
void update_entities(LevelData *level, Entity *player, const float dt);
void steer_towards(Entity *entity, Vector2 target, distance_threshold);
void update_summons(LevelData * level, Entity * player, const float dt);
void update_player(Player *player, GameState *state, float dt);

#endif
