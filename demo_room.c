

Vector2 DEMO_ROOM_POSITION = {3, 3};


void load_demo_room(LevelData *data) {
    data->entity_count = 2;
    data->entities = malloc(sizeof(Entity) * data->entity_count);
    data->entities[0] = create_skeleton1(Vector2Add(DEMO_ROOM_POSITION, (Vector2){1, 1}));
    data->entities[1] = create_skeleton1(Vector2Add(DEMO_ROOM_POSITION, (Vector2){4, 1}));

    // Setup walls
    data->n_collision_boxes = 18;
    data->collision_boxes = calloc(data->n_collision_boxes, sizeof(CollisionBox));
    // Top and bottom collision_boxes
    for (int i = 0; i < 6; i++) {
        data->collision_boxes[i].position = Vector2Add(DEMO_ROOM_POSITION, (Vector2){i, 0});
        data->collision_boxes[i + 6].position = Vector2Add(DEMO_ROOM_POSITION, (Vector2){i, 4});
    }

    for (int i = 0; i < 3; i++) {
        data->collision_boxes[i + 12].position = Vector2Add(DEMO_ROOM_POSITION, (Vector2){0, (i + 1)});
        data->collision_boxes[i + 15].position = Vector2Add(DEMO_ROOM_POSITION, (Vector2){5, (i + 1)});
    }
    for (int i = 0; i < data->n_collision_boxes; i++) {
        data->collision_boxes[i].is_active = true;
    }


    data->n_tiles = 36;
    data->tile_placements = calloc(data->n_tiles * 3, sizeof(int));
    for (int i = 0; i < data->n_tiles * 3; i++) {
        data->tile_placements[i] = -1;
    }

    // Top left corner
    data->tile_placements[0] = 0;
    data->tile_placements[1] = 0 + DEMO_ROOM_POSITION.x;
    data->tile_placements[2] = 0 + DEMO_ROOM_POSITION.y;

    // Top walls
    for (int i = 1; i < 5; i++) {
        data->tile_placements[i * 3 + 0] = 1;
        data->tile_placements[i * 3 + 1] = i + DEMO_ROOM_POSITION.x;
        data->tile_placements[i * 3 + 2] = 0 + DEMO_ROOM_POSITION.y;
    }
    // Top right corner
    data->tile_placements[5 * 3 + 0] = 5;
    data->tile_placements[5 * 3 + 1] = 5 + DEMO_ROOM_POSITION.x;
    data->tile_placements[5 * 3 + 2] = 0 + DEMO_ROOM_POSITION.y;

    // Left walls
    for (int i = 0; i < 4; i++) {
        data->tile_placements[(i + 6) * 3 + 0] = 10;
        data->tile_placements[(i + 6) * 3 + 1] = 0 + DEMO_ROOM_POSITION.x;
        data->tile_placements[(i + 6) * 3 + 2] = 1 + i + DEMO_ROOM_POSITION.y;
    }
    // Bottom left corner
    data->tile_placements[(4 + 6) * 3 + 0] = 40;
    data->tile_placements[(4 + 6) * 3 + 1] = 0 + DEMO_ROOM_POSITION.x;
    data->tile_placements[(4 + 6) * 3 + 2] = 4 + DEMO_ROOM_POSITION.y;

    // Right walls
    for (int i = 0; i < 4; i++) {
        data->tile_placements[(i + 11) * 3 + 0] = 15;
        data->tile_placements[(i + 11) * 3 + 1] = 5 + DEMO_ROOM_POSITION.x;
        data->tile_placements[(i + 11) * 3 + 2] = i + DEMO_ROOM_POSITION.y;
    }
    data->tile_placements[(4 + 11) * 3 + 0] = 45;
    data->tile_placements[(4 + 11) * 3 + 1] = 5 + DEMO_ROOM_POSITION.x;
    data->tile_placements[(4 + 11) * 3 + 2] = 4 + DEMO_ROOM_POSITION.y;

    // Bottom walls
    for (int i = 0; i < 4; i++) {
        data->tile_placements[(i + 16) * 3 + 0] = 41;
        data->tile_placements[(i + 16) * 3 + 1] = i + 1 + DEMO_ROOM_POSITION.x;
        data->tile_placements[(i + 16) * 3 + 2] = 4 + DEMO_ROOM_POSITION.y;
    }


    // Center tiles
    for (int i = 0; i < 3; i++) {
        for (int j = 1; j < 5; j++) {
            data->tile_placements[(i * 4 + j + 20) * 3 + 0] = 9; 
            data->tile_placements[(i * 4 + j + 20) * 3 + 1] = j + DEMO_ROOM_POSITION.x;
            data->tile_placements[(i * 4 + j + 20) * 3 + 2] = i + 1 + DEMO_ROOM_POSITION.y;
        }
    }

    data->items = NULL;
    data->n_items = 0;


    // Summons
    data->summons_count = 0;
    data->summons_capacity = 128;
    data->summons = malloc(sizeof(Entity) * data->summons_capacity);

    data->num_dead_summons = 0;


    data->debug_mode = false;
}

void draw_demo_room(LevelData *data, float scale, int frame_count) {
    draw(EL_ROOM, DEMO_ROOM_POSITION, scale);
    for (int i = 0; i < data->entity_count; i++) {
        Entity entity = data->entities[i];
        if (entity.is_dead) {
            continue;
        }
        animate(entity.sprite, frame_count, entity.position, scale);
    }

}


void unload_demo_room(LevelData *data) {
    for (int i = 0; i < data->entity_count; i++) {
        free_entity(&data->entities[i]);
    }
    free(data->entities);
}
